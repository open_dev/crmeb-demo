package com.ncamc.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ncamc.common.model.product.StoreProductAttrValue;

/**
 * 商品属性值表 Mapper 接口
 */
public interface StoreProductAttrValueDao extends BaseMapper<StoreProductAttrValue> {
}
