package com.ncamc.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ncamc.common.model.category.Category;

/**
 * 分类表 Mapper 接口
 */
public interface CategoryDao extends BaseMapper<Category> {
}
