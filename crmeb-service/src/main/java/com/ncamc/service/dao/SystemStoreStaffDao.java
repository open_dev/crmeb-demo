package com.ncamc.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ncamc.common.model.system.SystemStoreStaff;

/**
 * 门店店员表 Mapper 接口
 */
public interface SystemStoreStaffDao extends BaseMapper<SystemStoreStaff> {

}
