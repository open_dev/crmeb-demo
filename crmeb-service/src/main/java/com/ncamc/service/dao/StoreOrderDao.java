package com.ncamc.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ncamc.common.model.order.StoreOrder;

/**
 * 订单表 Mapper 接口
 */
public interface StoreOrderDao extends BaseMapper<StoreOrder> {
}
