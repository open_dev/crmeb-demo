package com.ncamc.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ncamc.common.model.product.StoreProductCoupon;

/**
 * Mapper 接口
 */
public interface StoreProductCouponDao extends BaseMapper<StoreProductCoupon> {
}
