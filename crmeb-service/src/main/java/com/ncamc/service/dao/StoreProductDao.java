package com.ncamc.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ncamc.common.model.product.StoreProduct;

/**
 * 商品表 Mapper 接口
 */
public interface StoreProductDao extends BaseMapper<StoreProduct> {
}
