package com.ncamc.service.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ncamc.common.model.system.SystemStore;

/**
 * 门店自提 Mapper 接口
 */
public interface SystemStoreDao extends BaseMapper<SystemStore> {
}
