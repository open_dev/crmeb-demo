package com.ncamc.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ncamc.common.model.product.StoreProductDescription;

/**
 * Mapper 接口
 *
 * @Author : hugaoqiang 2023-11-16
 */
public interface StoreProductDescriptionDao extends BaseMapper<StoreProductDescription> {
}
