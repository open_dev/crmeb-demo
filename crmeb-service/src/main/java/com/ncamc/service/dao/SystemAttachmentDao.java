package com.ncamc.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ncamc.common.model.system.SystemAttachment;

/**
 * 附件管理表 Mapper 接口
 */
public interface SystemAttachmentDao extends BaseMapper<SystemAttachment> {
}
