package com.ncamc.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ncamc.common.model.product.StoreProductAttr;

/**
 * 商品属性表 Mapper 接口
 */
public interface StoreProductAttrDao extends BaseMapper<StoreProductAttr> {
}
