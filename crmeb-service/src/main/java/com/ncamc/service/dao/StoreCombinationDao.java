package com.ncamc.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ncamc.common.model.combination.StoreCombination;

/**
 * 拼团商品表 Mapper 接口
 */
public interface StoreCombinationDao extends BaseMapper<StoreCombination> {
}
