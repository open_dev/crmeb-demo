package com.ncamc.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ncamc.common.model.article.Article;

/**
 * 文章管理表 Mapper 接口
 */
public interface ArticleDao extends BaseMapper<Article> {
}
