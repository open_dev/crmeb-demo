package com.ncamc.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ncamc.common.model.system.SystemAdmin;

/**
 * 后台管理员表 Mapper 接口
 */
public interface SystemAdminDao extends BaseMapper<SystemAdmin> {
}
