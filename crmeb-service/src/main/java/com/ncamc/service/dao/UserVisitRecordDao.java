package com.ncamc.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ncamc.common.model.record.UserVisitRecord;

/**
 * 用户访问记录表 Mapper 接口
 */
public interface UserVisitRecordDao extends BaseMapper<UserVisitRecord> {
}
