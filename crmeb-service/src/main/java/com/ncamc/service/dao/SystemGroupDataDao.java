package com.ncamc.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ncamc.common.model.system.SystemGroupData;

/**
 * 组合数据详情表 Mapper 接口
 */
public interface SystemGroupDataDao extends BaseMapper<SystemGroupData> {
}
