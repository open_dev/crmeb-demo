package com.ncamc.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ncamc.common.model.bargain.StoreBargain;

/**
 * 砍价表 Mapper 接口
 */
public interface StoreBargainDao extends BaseMapper<StoreBargain> {
}
