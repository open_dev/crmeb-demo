package com.ncamc.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ncamc.common.model.user.User;

/**
 * 用户表 Mapper 接口
 */
public interface UserDao extends BaseMapper<User> {
}
