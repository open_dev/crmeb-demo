package com.ncamc.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ncamc.common.model.order.StoreOrderInfo;

/**
 * 订单购物详情表 Mapper 接口
 *
 * @Author : hugaoqiang 2023-11-10
 */
public interface StoreOrderInfoDao extends BaseMapper<StoreOrderInfo> {
}
