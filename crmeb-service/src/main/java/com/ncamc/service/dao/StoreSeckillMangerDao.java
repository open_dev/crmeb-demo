package com.ncamc.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ncamc.common.model.seckill.StoreSeckillManger;

/**
 * Mapper 接口
 */
public interface StoreSeckillMangerDao extends BaseMapper<StoreSeckillManger> {
}
