package com.ncamc.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ncamc.common.model.seckill.StoreSeckill;

/**
 * 商品秒杀产品表 Mapper 接口
 *
 * @Author : hugaoqiang 2023-11-10
 */
public interface StoreSeckillDao extends BaseMapper<StoreSeckill> {
}
