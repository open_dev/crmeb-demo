package com.ncamc.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ncamc.common.model.system.SystemConfig;

/**
 * 配置表 Mapper 接口
 */
public interface SystemConfigDao extends BaseMapper<SystemConfig> {
}
