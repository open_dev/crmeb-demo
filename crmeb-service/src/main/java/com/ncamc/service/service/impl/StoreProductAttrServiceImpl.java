package com.ncamc.service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ncamc.common.model.product.StoreProductAttr;
import com.ncamc.service.dao.StoreProductAttrDao;
import com.ncamc.service.service.StoreProductAttrService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * StoreProductAttrServiceImpl 接口实现
 *
 * @Author : hugaoqiang 2023-11-16
 */
@Service
public class StoreProductAttrServiceImpl extends ServiceImpl<StoreProductAttrDao, StoreProductAttr> implements StoreProductAttrService {

    @Resource
    private StoreProductAttrDao dao;

    /**
     * 根据基本属性查询商品属性详情
     *
     * @param storeProductAttr 商品属性
     * @return 查询商品属性集合
     */
    @Override
    public List<StoreProductAttr> getByEntity(StoreProductAttr storeProductAttr) {
        LambdaQueryWrapper<StoreProductAttr> lqw = Wrappers.lambdaQuery();
        if (null != storeProductAttr.getId())
            lqw.eq(StoreProductAttr::getId, storeProductAttr.getId());
        if (StringUtils.isNotBlank(storeProductAttr.getAttrName()))
            lqw.eq(StoreProductAttr::getAttrName, storeProductAttr.getAttrName());
        if (null != storeProductAttr.getProductId())
            lqw.eq(StoreProductAttr::getProductId, storeProductAttr.getProductId());
        if (null != storeProductAttr.getType())
            lqw.eq(StoreProductAttr::getType, storeProductAttr.getType());
        return dao.selectList(lqw);
    }
}
