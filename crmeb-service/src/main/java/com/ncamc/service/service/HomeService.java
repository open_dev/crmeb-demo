package com.ncamc.service.service;

import com.ncamc.common.response.HomeRateResponse;
import com.ncamc.common.vo.MyRecord;

import java.util.List;
import java.util.Map;

/**
 * 首页统计
 */
public interface HomeService {

    /**
     * 用户曲线图
     */
    Map<Object, Object> chartUser();

    /**
     * 30天订单量趋势
     */
    Map<String, Object> chartOrder();

    /**
     * 首页数据
     *
     * @return HomeRateResponse
     */
    HomeRateResponse indexDate();
}
