package com.ncamc.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ncamc.common.model.bargain.StoreBargain;

import java.util.List;

/**
 * 砍价 Service
 */
public interface StoreBargainService extends IService<StoreBargain> {

    /**
     * 获取当前时间的砍价商品
     *
     * @return List<StoreBargain>
     */
    List<StoreBargain> getCurrentBargainByProductId(Integer productId);

}
