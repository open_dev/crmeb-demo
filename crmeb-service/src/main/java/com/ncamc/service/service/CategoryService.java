package com.ncamc.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ncamc.common.model.category.Category;
import com.ncamc.common.vo.CategoryTreeVo;

import java.util.List;

/**
 * CategoryService 接口
 */
public interface CategoryService extends IService<Category> {

    /**
     * 通过id集合获取列表
     *
     * @param idList List<Integer> id集合
     * @return List<Category>
     */
    List<Category> getByIds(List<Integer> idList);

    /**
     * 获取树形结构数据
     *
     * @param type   分类
     * @param status 状态
     * @param name   名称
     * @return List
     */
    List<CategoryTreeVo> getListTree(Integer type, Integer status, String name);
}
