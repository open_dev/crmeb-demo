package com.ncamc.service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ncamc.common.model.product.StoreProductRelation;
import com.ncamc.service.dao.StoreProductRelationDao;
import com.ncamc.service.service.StoreProductRelationService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * StoreProductRelationServiceImpl 接口实现
 *
 * @Author : hugaoqiang 2023-11-16
 */
@Service
public class StoreProductRelationServiceImpl extends ServiceImpl<StoreProductRelationDao, StoreProductRelation> implements StoreProductRelationService {

    @Resource
    private StoreProductRelationDao dao;

    /**
     * 根据产品id和类型获取对应列表
     *
     * @param productId 产品id
     * @param type      类型
     * @return 对应结果
     */
    @Override
    public List<StoreProductRelation> getList(Integer productId, String type) {
        LambdaQueryWrapper<StoreProductRelation> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(StoreProductRelation::getProductId, productId);
        lambdaQueryWrapper.eq(StoreProductRelation::getType, type);
        return dao.selectList(lambdaQueryWrapper);
    }
}
