package com.ncamc.service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ncamc.common.model.record.UserVisitRecord;
import com.ncamc.service.dao.UserVisitRecordDao;
import com.ncamc.service.service.UserVisitRecordService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * UserVisitRecordServiceImpl 接口实现
 *
 * @Author : hugaoqiang 2023-11-15
 */
@Service
public class UserVisitRecordServiceImpl extends ServiceImpl<UserVisitRecordDao, UserVisitRecord> implements UserVisitRecordService {

    @Resource
    private UserVisitRecordDao dao;

    /**
     * 通过日期获取浏览量
     *
     * @param date 日期
     * @return Integer
     */
    @Override
    public Integer getPageviewsByDate(String date) {
        QueryWrapper<UserVisitRecord> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("id");
        queryWrapper.eq("date", date);
        return dao.selectCount(queryWrapper);
    }
}
