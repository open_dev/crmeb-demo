package com.ncamc.service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ncamc.common.model.system.SystemStore;
import com.ncamc.service.dao.SystemStoreDao;
import com.ncamc.service.service.SystemStoreService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * SystemStoreServiceImpl 接口实现
 *
 * @Author : hugaoqiang 2023-11-14
 */
@Service
public class SystemStoreServiceImpl extends ServiceImpl<SystemStoreDao, SystemStore> implements SystemStoreService {

    @Resource
    private SystemStoreDao dao;

    /**
     * 根据id集合查询数据，返回 map
     *
     * @param storeIdList List<Integer> id集合
     * @return HashMap<Integer, SystemStore>
     */
    @Override
    public Map<Integer, SystemStore> getMaoInId(List<Integer> storeIdList) {
        Map<Integer, SystemStore> map = new HashMap<>();
        if (storeIdList.isEmpty()) return map;

        LambdaQueryWrapper<SystemStore> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.in(SystemStore::getId, storeIdList);
        List<SystemStore> systemStoreList = dao.selectList(lambdaQueryWrapper);
        if (systemStoreList.isEmpty()) return map;

        for (SystemStore systemStore : systemStoreList) {
            map.put(systemStore.getId(), systemStore);
        }
        return map;
    }
}
