package com.ncamc.service.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ncamc.common.model.article.Article;
import com.ncamc.service.dao.ArticleDao;
import com.ncamc.service.service.ArticleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Author : hugaoqiang 2023-11-10
 */
@Service
public class ArticleServiceImpl extends ServiceImpl<ArticleDao, Article> implements ArticleService {
    @Resource
    private ArticleDao dao;
}
