package com.ncamc.service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ncamc.common.constants.Constants;
import com.ncamc.common.exception.CrmebException;
import com.ncamc.common.model.user.User;
import com.ncamc.common.token.FrontTokenComponent;
import com.ncamc.common.utils.DateUtil;
import com.ncamc.common.vo.dateLimitUtilVo;
import com.ncamc.service.dao.UserDao;
import com.ncamc.service.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author : hugaoqiang 2023-11-10
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserDao, User> implements UserService {

    @Resource
    private UserDao userDao;

    @Resource
    private FrontTokenComponent tokenComponent;

    /**
     * 获取当前用户 ID
     *
     * @return Integer
     */
    @Override
    public Integer getUserIdException() {
        Integer id = tokenComponent.getUserId();
        if (id == null) {
            throw new CrmebException("登录信息已过期，请重新登录! ");
        }
        return id;
    }

    /**
     * 用户id in list
     *
     * @param uidList List<Integer> id
     */
    private List<User> getListInUid(List<Integer> uidList) {
        LambdaQueryWrapper<User> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.in(User::getUid, uidList);
        return userDao.selectList(lambdaQueryWrapper);
    }

    /**
     * 根据用户id获取用户列表 map模式
     *
     * @param uidList uidList
     * @return HashMap
     */
    @Override
    public Map<Integer, User> getMapListInUid(List<Integer> uidList) {
        List<User> userList = getListInUid(uidList);
        return getMapByList(userList);
    }

    /**
     * 根据日期获取注册用户数量
     *
     * @param date 日期
     * @return Integer
     */
    @Override
    public Integer getRegisterNumByDate(String date) {
        QueryWrapper<User> queryWrapper = Wrappers.query();
        queryWrapper.select("uid");
        queryWrapper.apply("date_format(create_time, '%Y-%m-%d') = {0}", date);
        return userDao.selectCount(queryWrapper);
    }

    /**
     * 按开始结束时间查询每日新增用户数量
     *
     * @param date String 时间范围
     * @return HashMap<String, Object>
     */
    @Override
    public Map<Object, Object> getAddUserCountGroupDate(String date) {
        Map<Object, Object> map = new HashMap<>();
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("count(uid) as uid", "left(create_time, 10) as create_time");
        if (StringUtils.isNotBlank(date)) {
            dateLimitUtilVo dateLimit = DateUtil.getDateLimit(date);
            queryWrapper.between("create_time", dateLimit.getStartTime(), dateLimit.getEndTime());
        }
        queryWrapper.groupBy("left(create_time, 10)").orderByAsc("create_time");
        List<User> list = userDao.selectList(queryWrapper);
        if (list.isEmpty()) {
            return map;
        }

        for (User user : list) {
            map.put(DateUtil.dateToStr(user.getCreateTime(), Constants.DATE_FORMAT_DATE), user.getUid());
        }
        return map;
    }

    /**
     * 根据用户id获取用户列表 map模式
     *
     * @return HashMap<Integer, User>
     */
    private Map<Integer, User> getMapByList(List<User> list) {
        Map<Integer, User> map = new HashMap<>();
        if (null == list || list.isEmpty()) {
            return map;
        }

        for (User user : list) {
            map.put(user.getUid(), user);
        }

        return map;
    }
}
