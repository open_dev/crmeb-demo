package com.ncamc.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ncamc.common.model.record.UserVisitRecord;

/**
 * UserVisitRecordService 接口
 */
public interface UserVisitRecordService extends IService<UserVisitRecord> {

    /**
     * 通过日期获取浏览量
     *
     * @param date 日期
     * @return Integer
     */
    Integer getPageviewsByDate(String date);
}
