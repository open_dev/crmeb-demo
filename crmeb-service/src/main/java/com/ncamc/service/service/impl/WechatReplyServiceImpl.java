package com.ncamc.service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ncamc.common.model.wechat.WechatReply;
import com.ncamc.service.dao.WechatReplyDao;
import com.ncamc.service.service.WechatReplyService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Author : hugaoqiang 2023-11-10
 */
@Service
public class WechatReplyServiceImpl extends ServiceImpl<WechatReplyDao, WechatReply> implements WechatReplyService {

    @Resource
    private WechatReplyDao dao;

    /**
     * 根据关键字查询数据
     *
     * @param keywords 新增参数
     * @return WechatReply
     */
    @Override
    public WechatReply getVoByKeywords(String keywords) {
        // 检测重复
        LambdaQueryWrapper<WechatReply> objectLambdaQueryWrapper = new LambdaQueryWrapper<>();
        objectLambdaQueryWrapper.eq(WechatReply::getKeywords, keywords);
        return dao.selectOne(objectLambdaQueryWrapper);
    }
}
