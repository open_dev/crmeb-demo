package com.ncamc.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ncamc.common.model.seckill.StoreSeckillManger;
import com.ncamc.common.response.StoreSeckillManagerResponse;

import java.util.List;

/**
 * StoreSeckillMangerService 接口
 */
public interface StoreSeckillMangerService extends IService<StoreSeckillManger> {

    /**
     * 获取所有秒杀配置
     *
     * @return List
     */
    List<StoreSeckillManagerResponse> getAllList();
}
