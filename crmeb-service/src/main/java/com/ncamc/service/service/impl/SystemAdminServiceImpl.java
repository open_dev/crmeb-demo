package com.ncamc.service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ncamc.service.service.SystemAdminService;
import com.ncamc.service.dao.SystemAdminDao;
import com.ncamc.common.model.system.SystemAdmin;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Author : hugaoqiang 2023-09-22
 */
@Service
public class SystemAdminServiceImpl extends ServiceImpl<SystemAdminDao, SystemAdmin> implements SystemAdminService {

    @Resource
    private SystemAdminDao dao;

    /**
     * 通过用户名获取用户
     *
     * @param username 用户名
     * @return SystemAdmin
     */
    @Override
    public SystemAdmin selectUserByUserName(String username) {
        LambdaQueryWrapper<SystemAdmin> lqw = Wrappers.lambdaQuery();
        lqw.eq(SystemAdmin::getAccount, username);
        lqw.eq(SystemAdmin::getIsDel, false);
        return dao.selectOne(lqw);
    }
}
