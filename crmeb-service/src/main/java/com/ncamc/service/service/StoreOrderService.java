package com.ncamc.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ncamc.common.model.order.StoreOrder;

import java.math.BigDecimal;
import java.util.List;

/**
 * StoreOrderService 接口
 */
public interface StoreOrderService extends IService<StoreOrder> {

    /**
     * 按开始结束时间分组订单
     *
     * @param dateLimit String 时间范围
     * @param lefTime   int 截取创建时间长度
     * @return HashMap<String, Object>
     */
    List<StoreOrder> getOrderGroupByDate(String dateLimit, int lefTime);

    /**
     * 通过日期获取订单数量
     * @param date 日期，yyyy-MM-dd格式
     * @return Integer
     */
    Integer getOrderNumByDate(String date);

    /**
     * 通过日期获取支付订单金额
     *
     * @param date 日期，yyyy-MM-dd格式
     * @return BigDecimal
     */
    BigDecimal getPayOrderAmountByDate(String date);
}
