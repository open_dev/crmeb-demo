package com.ncamc.service.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ncamc.common.model.product.StoreProductDescription;
import com.ncamc.service.dao.StoreProductDescriptionDao;
import com.ncamc.service.service.StoreProductDescriptionService;
import org.springframework.stereotype.Service;

/**
 * StoreProductDescriptionServiceImpl 接口实现
 *
 * @Author : hugaoqiang 2023-11-16
 */
@Service
public class StoreProductDescriptionServiceImpl extends ServiceImpl<StoreProductDescriptionDao, StoreProductDescription> implements StoreProductDescriptionService {

}
