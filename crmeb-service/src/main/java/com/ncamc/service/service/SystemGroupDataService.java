package com.ncamc.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ncamc.common.request.PageParamRequest;
import com.ncamc.common.request.SystemGroupDataSearchRequest;
import com.ncamc.common.model.system.SystemGroupData;

import java.util.List;

/**
 * SystemGroupDataService 接口
 */
public interface SystemGroupDataService extends IService<SystemGroupData> {

    /**
     * 分页组合数据详情
     *
     * @param request          搜索条件
     * @param pageParamRequest 分页参数
     */
    List<SystemGroupData> getList(SystemGroupDataSearchRequest request, PageParamRequest pageParamRequest);

    /**
     * 通过gid获取列表 推荐二开使用
     *
     * @param gid Integer group id
     * @return List<T>
     */
    <T> List<T> getListByGid(Integer gid, Class<T> cls);
}
