package com.ncamc.service.service;

import com.ncamc.common.model.combination.StoreCombination;

import java.util.List;

/**
 * StorePinkService
 */
public interface StoreCombinationService {

    /**
     * 获取当前时间的拼团商品
     */
    List<StoreCombination> getCurrentBargainByProductId(Integer productId);

}
