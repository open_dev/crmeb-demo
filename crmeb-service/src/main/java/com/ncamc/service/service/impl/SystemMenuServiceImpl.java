package com.ncamc.service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ncamc.common.model.system.SystemMenu;
import com.ncamc.common.utils.RedisUtil;
import com.ncamc.service.dao.SystemMenuDao;
import com.ncamc.service.service.SystemMenuService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author : hugaoqiang 2023-09-22
 */
@Service
public class SystemMenuServiceImpl extends ServiceImpl<SystemMenuDao, SystemMenu> implements SystemMenuService {

    @Resource
    private SystemMenuDao dao;

    @Resource
    private RedisUtil redisUtil;

    /**
     * 获取所有菜单
     *
     * @return List<SystemMenu>
     */
    @Override
    public List<SystemMenu> findAllCatalogue() {
        LambdaQueryWrapper<SystemMenu> lqw = Wrappers.lambdaQuery();
        lqw.eq(SystemMenu::getIsDelte, false);
        lqw.eq(SystemMenu::getIsShow, true);
        lqw.ne(SystemMenu::getMenuType, "A");
        return dao.selectList(lqw);
    }

    /**
     * 获取所有权限
     *
     * @return List
     */
    @Override
    public List<SystemMenu> getAllPermissions() {
        LambdaQueryWrapper<SystemMenu> lqw = Wrappers.lambdaQuery();
        lqw.eq(SystemMenu::getIsDelte, false);
        lqw.ne(SystemMenu::getMenuType, "M");
        return dao.selectList(lqw);
    }

    /**
     * 通过用户id获取权限
     *
     * @param userId 用户id
     * @return List
     */
    @Override
    public List<SystemMenu> findPermissionByUserId(Integer userId) {
        return dao.findPermissionByUserId(userId);
    }

    /**
     * 获取用户路由
     *
     * @param userId 用户id
     * @return List
     */
    @Override
    public List<SystemMenu> getMenusByUserId(Integer userId) {
        return dao.getMenusByUserId(userId);
    }
}
