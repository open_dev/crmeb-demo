package com.ncamc.service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ncamc.common.model.order.StoreOrder;
import com.ncamc.common.utils.DateUtil;
import com.ncamc.common.vo.dateLimitUtilVo;
import com.ncamc.service.dao.StoreOrderDao;
import com.ncamc.service.service.StoreOrderService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

/**
 * StoreOrderServiceImpl 接口实现
 *
 * @Author : hugaoqiang 2023-11-10
 */
@Service
public class StoreOrderServiceImpl extends ServiceImpl<StoreOrderDao, StoreOrder> implements StoreOrderService {

    @Resource
    private StoreOrderDao dao;

    /**
     * 按开始结束时间分组订单
     *
     * @param date    String 时间范围
     * @param lefTime int 截取创建时间长度
     * @return HashMap<String, Object>
     */
    @Override
    public List<StoreOrder> getOrderGroupByDate(String date, int lefTime) {
        QueryWrapper<StoreOrder> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("sum(pay_price) as pay_price", "left(create_time, " + lefTime + ") as orderId", "count(id) as id");
        if (StringUtils.isNotBlank(date)) {
            dateLimitUtilVo dateLimit = DateUtil.getDateLimit(date);
            queryWrapper.between("create_time", dateLimit.getStartTime(), dateLimit.getEndTime());
        }
        queryWrapper.groupBy("orderId").orderByAsc("orderId");
        return dao.selectList(queryWrapper);
    }

    /**
     * 通过日期获取订单数量
     *
     * @param date 日期，yyyy-MM-dd格式
     * @return Integer
     */
    @Override
    public Integer getOrderNumByDate(String date) {
        QueryWrapper<StoreOrder> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("id");
        queryWrapper.eq("paid", 1);
        queryWrapper.apply("date_format(create_time, '%Y-%m-%d') = {0}", date);
        return dao.selectCount(queryWrapper);
    }

    /**
     * 通过日期获取支付订单金额
     *
     * @param date 日期，yyyy-MM-dd格式
     * @return BigDecimal
     */
    @Override
    public BigDecimal getPayOrderAmountByDate(String date) {
        QueryWrapper<StoreOrder> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("IFNULL(sum(pay_price), 0) as pay_price");
        queryWrapper.eq("paid", 1);
        queryWrapper.apply("date_format(create_time, '%Y-%m-%d') = {0}", date);
        StoreOrder storeOrder = dao.selectOne(queryWrapper);
        return storeOrder.getPayPrice();
    }
}
