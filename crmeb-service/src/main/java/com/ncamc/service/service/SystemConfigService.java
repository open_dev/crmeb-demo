package com.ncamc.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ncamc.common.model.system.SystemConfig;

/**
 * SystemConfigService 接口
 */
public interface SystemConfigService extends IService<SystemConfig> {

    /**
     * 根据menu name 获取 value
     *
     * @param key menu name
     * @return String
     */
    String getValueByKey(String key);

    /**
     * 根据 name 获取 value 找不到抛异常
     *
     * @param key menu name
     * @return String
     */
    String getValueByKeyException(String key);

}
