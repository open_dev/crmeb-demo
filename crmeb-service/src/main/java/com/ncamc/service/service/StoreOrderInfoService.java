package com.ncamc.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ncamc.common.model.order.StoreOrderInfo;
import com.ncamc.common.vo.StoreOrderInfoOldVo;

import java.util.List;

public interface StoreOrderInfoService extends IService<StoreOrderInfo> {
    List<StoreOrderInfoOldVo> getOrderListByOrderId(int orderId);
}
