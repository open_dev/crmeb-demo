package com.ncamc.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ncamc.common.model.system.SystemAttachment;

public interface SystemAttachmentService extends IService<SystemAttachment> {

    /**
     * 给图片加前缀
     *
     * @param path String 路径
     * @return String
     */
    String prefixImage(String path);

    /**
     * 给文件加前缀
     *
     * @param path String 路径
     * @return String
     */
    String prefixFile(String path);

}
