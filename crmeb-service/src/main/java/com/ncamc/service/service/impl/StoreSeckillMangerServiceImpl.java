package com.ncamc.service.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ncamc.common.model.seckill.StoreSeckillManger;
import com.ncamc.common.response.StoreSeckillManagerResponse;
import com.ncamc.service.dao.StoreSeckillMangerDao;
import com.ncamc.service.service.StoreSeckillMangerService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * StoreSeckillMangerServiceImpl 接口实现
 *
 * @Author : hugaoqiang 2023-11-10
 */
@Service
public class StoreSeckillMangerServiceImpl extends ServiceImpl<StoreSeckillMangerDao, StoreSeckillManger> implements StoreSeckillMangerService {

    @Resource
    private StoreSeckillMangerDao dao;

    /**
     * 获取所有秒杀配置
     *
     * @return List
     */
    @Override
    public List<StoreSeckillManagerResponse> getAllList() {
        LambdaQueryWrapper<StoreSeckillManger> lambdaQueryWrapper = Wrappers.lambdaQuery();
        lambdaQueryWrapper.orderByAsc(StoreSeckillManger::getSort);
        // 处理数据 time 格式 适配前端
        List<StoreSeckillManger> storeSeckillMangers = dao.selectList(lambdaQueryWrapper);
        if (CollUtil.isEmpty(storeSeckillMangers)) {
            return CollUtil.newArrayList();
        }
        List<StoreSeckillManagerResponse> responses = new ArrayList<>();
        convertTime(responses, storeSeckillMangers);
        return responses;
    }

    // 列表用 格式化time 对前端输出一致
    private void convertTime(List<StoreSeckillManagerResponse> responses, List<StoreSeckillManger> storeSeckillMangers) {
        storeSeckillMangers.forEach(e -> {
            StoreSeckillManagerResponse r = new StoreSeckillManagerResponse();
            BeanUtils.copyProperties(e, r);
            cTime(e, r);
            responses.add(r);
        });
    }

    // 详情用 格式化time 对前端输出一致
    private void cTime(StoreSeckillManger e, StoreSeckillManagerResponse r) {
        String pStartTime = e.getStartTime().toString();
        String pEndTime = e.getEndTime().toString();
        String startTime = pStartTime.length() == 1 ? "0" + pStartTime : pStartTime;
        String endTime = pEndTime.length() == 1 ? "0" + pEndTime : pEndTime;
        r.setTime(startTime + ":00," + endTime + ":00");
    }
}
