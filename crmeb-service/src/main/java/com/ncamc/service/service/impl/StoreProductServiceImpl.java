package com.ncamc.service.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ncamc.common.constants.Constants;
import com.ncamc.common.model.category.Category;
import com.ncamc.common.model.product.StoreProduct;
import com.ncamc.common.model.product.StoreProductAttr;
import com.ncamc.common.model.product.StoreProductAttrValue;
import com.ncamc.common.model.product.StoreProductDescription;
import com.ncamc.common.page.CommonPage;
import com.ncamc.common.request.PageParamRequest;
import com.ncamc.common.request.StoreProductSearchRequest;
import com.ncamc.common.response.StoreProductAttrValueResponse;
import com.ncamc.common.response.StoreProductResponse;
import com.ncamc.common.response.StoreProductTabsHeader;
import com.ncamc.common.utils.CrmebUtil;
import com.ncamc.service.dao.StoreProductDao;
import com.ncamc.service.service.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author : hugaoqiang 2023-11-16
 */
@Service
public class StoreProductServiceImpl extends ServiceImpl<StoreProductDao, StoreProduct> implements StoreProductService {

    @Resource
    private StoreProductDao dao;

    @Resource
    private SystemConfigService systemConfigService;

    @Resource
    private StoreProductAttrService attrService;

    @Resource
    private StoreProductAttrValueService storeProductAttrValueService;

    @Resource
    private StoreProductDescriptionService storeProductDescriptionService;

    @Resource
    private CategoryService categoryService;

    @Resource
    private StoreProductRelationService storeProductRelationService;

    /**
     * 获取产品列表Admin
     *
     * @param request          筛选参数
     * @param pageParamRequest 分页参数
     * @return PageInfo
     */
    @Override
    public PageInfo<StoreProductResponse> getAdminList(StoreProductSearchRequest request, PageParamRequest pageParamRequest) {
        // 带 StoreProduct 类多条件查询
        LambdaQueryWrapper<StoreProduct> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        queryCondition(new StoreProductTabsHeader(), request, lambdaQueryWrapper);

        // 关键字搜索
        if (StrUtil.isNotBlank(request.getKeywords())) {
            lambdaQueryWrapper.and(i -> i
                    .or().eq(StoreProduct::getId, request.getKeywords())
                    .or().like(StoreProduct::getStoreName, request.getKeywords())
                    .or().like(StoreProduct::getKeyword, request.getKeywords()));
        }
        lambdaQueryWrapper.apply(StringUtils.isNotBlank(request.getCateId()), "FIND_IN_SET ('" + request.getCateId() + "', cate_id)");
        lambdaQueryWrapper.orderByDesc(StoreProduct::getSort).orderByDesc(StoreProduct::getId);

        Page<StoreProduct> storeProductPage = PageHelper.startPage(pageParamRequest.getPage(), pageParamRequest.getLimit());
        List<StoreProduct> storeProducts = dao.selectList(lambdaQueryWrapper);
        List<StoreProductResponse> storeProductResponses = new ArrayList<>();
        for (StoreProduct product : storeProducts) {
            StoreProductResponse storeProductResponse = new StoreProductResponse();
            BeanUtils.copyProperties(product, storeProductResponse);
            StoreProductAttr storeProductAttrPram = new StoreProductAttr();
            storeProductAttrPram.setProductId(product.getId()).setType(Constants.PRODUCT_TYPE_NORMAL);
            List<StoreProductAttr> attrs = attrService.getByEntity(storeProductAttrPram);

            if (!attrs.isEmpty()) {
                storeProductResponse.setAttr(attrs);
            }

            List<StoreProductAttrValueResponse> storeProductAttrValueResponse = new ArrayList<>();
            StoreProductAttrValue storeProductAttrValuePram = new StoreProductAttrValue();
            storeProductAttrValuePram.setProductId(product.getId()).setType(Constants.PRODUCT_TYPE_NORMAL);
            List<StoreProductAttrValue> storeProductAttrValues = storeProductAttrValueService.getByEntity(storeProductAttrValuePram);
            storeProductAttrValues.stream().map(e -> {
                StoreProductAttrValueResponse response = new StoreProductAttrValueResponse();
                BeanUtils.copyProperties(e, response);
                storeProductAttrValueResponse.add(response);
                return e;
            }).collect(Collectors.toList());
            storeProductResponse.setAttrValue(storeProductAttrValueResponse);

            // 处理副文本
            StoreProductDescription sd = storeProductDescriptionService.getOne(
                    new LambdaQueryWrapper<StoreProductDescription>()
                            .eq(StoreProductDescription::getProductId, product.getId())
                            .eq(StoreProductDescription::getType, Constants.PRODUCT_TYPE_NORMAL));

            if (null != sd) {
                storeProductResponse.setContent(null == sd.getDescription() ? "" : sd.getDescription());
            }

            // 处理分类中文
            List<Category> cg = categoryService.getByIds(CrmebUtil.stringToArray(product.getCateId()));
            storeProductResponse.setCateValues(CollUtil.isEmpty(cg) ? Constants.STR_EMPTY : cg.stream().map(Category::getName).collect(Collectors.joining()));
            storeProductResponse.setCollectCount(storeProductRelationService.getList(product.getId(), "collect").size());
            storeProductResponses.add(storeProductResponse);
        }

        // 多条sql查询处理分页正确
        return CommonPage.copyPageInfo(storeProductPage, storeProductResponses);
    }

    /**
     * 获取tabsHeader对应数量
     *
     * @return List
     */
    @Override
    public List<StoreProductTabsHeader> getTabsHeader() {
        List<StoreProductTabsHeader> headers = getStoreProductTabsHeaders();
        for (StoreProductTabsHeader header : headers) {
            LambdaQueryWrapper<StoreProduct> lambdaQueryWrapper = new LambdaQueryWrapper<>();
            queryCondition(header, new StoreProductSearchRequest(), lambdaQueryWrapper);
            List<StoreProduct> storeProducts = dao.selectList(lambdaQueryWrapper);
            header.setCount(storeProducts.size());
        }
        return headers;
    }

    private static List<StoreProductTabsHeader> getStoreProductTabsHeaders() {
        List<StoreProductTabsHeader> headers = new ArrayList<>();
        StoreProductTabsHeader header1 = new StoreProductTabsHeader(0, "出售中商品", 1);
        StoreProductTabsHeader header2 = new StoreProductTabsHeader(0, "仓库中商品", 2);
        StoreProductTabsHeader header3 = new StoreProductTabsHeader(0, "已经售馨商品", 3);
        StoreProductTabsHeader header4 = new StoreProductTabsHeader(0, "警戒商品", 4);
        StoreProductTabsHeader header5 = new StoreProductTabsHeader(0, "商品回收站", 5);
        headers.add(header1);
        headers.add(header2);
        headers.add(header3);
        headers.add(header4);
        headers.add(header5);
        return headers;
    }

    /**
     * 设置查询条件
     *
     * @param header             StoreProductTabsHeader 对象
     * @param request            StoreProductSearchRequest 对象
     * @param lambdaQueryWrapper LambdaQueryWrapper 条件构造器
     */
    private void queryCondition(StoreProductTabsHeader header, StoreProductSearchRequest request, LambdaQueryWrapper<StoreProduct> lambdaQueryWrapper) {
        Integer t = header.getType();
        if (t == null)
            t = request.getType();
        // 类型搜索
        switch (t) {
            case 1:
                //出售中（已上架）
                lambdaQueryWrapper.eq(StoreProduct::getIsShow, true);
                lambdaQueryWrapper.eq(StoreProduct::getIsRecycle, false);
                lambdaQueryWrapper.eq(StoreProduct::getIsDel, false);
                break;
            case 2:
                //仓库中（未上架）
                lambdaQueryWrapper.eq(StoreProduct::getIsShow, false);
                lambdaQueryWrapper.eq(StoreProduct::getIsRecycle, false);
                lambdaQueryWrapper.eq(StoreProduct::getIsDel, false);
                break;
            case 3:
                //已售罄
                lambdaQueryWrapper.eq(StoreProduct::getStock, 0);
                lambdaQueryWrapper.eq(StoreProduct::getIsRecycle, false);
                lambdaQueryWrapper.eq(StoreProduct::getIsDel, false);
                break;
            case 4:
                //警戒库存
                int stock = Integer.parseInt(systemConfigService.getValueByKey("store_stock"));
                lambdaQueryWrapper.eq(StoreProduct::getStock, stock);
                lambdaQueryWrapper.eq(StoreProduct::getIsRecycle, false);
                lambdaQueryWrapper.eq(StoreProduct::getIsDel, false);
                break;
            case 5:
                //回收站
                lambdaQueryWrapper.eq(StoreProduct::getIsRecycle, true);
                lambdaQueryWrapper.eq(StoreProduct::getIsDel, false);
                break;
            default:
                break;
        }
    }
}
