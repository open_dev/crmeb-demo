package com.ncamc.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ncamc.common.model.product.StoreProductRelation;

import java.util.List;

/**
 * StoreProductRelationService 接口
 */
public interface StoreProductRelationService extends IService<StoreProductRelation> {

    /**
     * 根据产品id和类型获取对应列表
     *
     * @param productId 产品id
     * @param type      类型
     * @return 对应结果
     */
    List<StoreProductRelation> getList(Integer productId, String type);
}
