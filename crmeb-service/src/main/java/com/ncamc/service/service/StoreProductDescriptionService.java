package com.ncamc.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ncamc.common.model.product.StoreProductDescription;

/**
 * StoreProductDescriptionService 接口
 */
public interface StoreProductDescriptionService extends IService<StoreProductDescription> {
}
