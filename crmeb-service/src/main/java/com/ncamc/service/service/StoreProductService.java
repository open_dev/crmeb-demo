package com.ncamc.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.ncamc.common.model.product.StoreProduct;
import com.ncamc.common.request.PageParamRequest;
import com.ncamc.common.request.StoreProductSearchRequest;
import com.ncamc.common.response.StoreProductResponse;
import com.ncamc.common.response.StoreProductTabsHeader;

import java.util.List;

/**
 * StoreProductService 接口
 */
public interface StoreProductService extends IService<StoreProduct> {

    /**
     * 获取产品列表Admin
     *
     * @param request          筛选参数
     * @param pageParamRequest 分页参数
     * @return PageInfo
     */
    PageInfo<StoreProductResponse> getAdminList(StoreProductSearchRequest request, PageParamRequest pageParamRequest);

    /**
     * 获取tabsHeader对应数量
     *
     * @return List
     */
    List<StoreProductTabsHeader> getTabsHeader();

}
