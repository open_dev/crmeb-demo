package com.ncamc.service.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ncamc.common.config.CrmebConfig;
import com.ncamc.common.constants.Constants;
import com.ncamc.common.exception.CrmebException;
import com.ncamc.common.model.system.SystemConfig;
import com.ncamc.common.utils.RedisUtil;
import com.ncamc.service.dao.SystemConfigDao;
import com.ncamc.service.service.SystemConfigService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author : hugaoqiang 2023-09-25
 */
@Service
public class SystemConfigServiceImpl extends ServiceImpl<SystemConfigDao, SystemConfig> implements SystemConfigService {

    private static final String redisKey = Constants.CONFIG_LIST;
    @Resource
    private SystemConfigDao dao;
    @Resource
    private RedisUtil redisUtil;
    @Resource
    private CrmebConfig crmebConfig;

    /**
     * 根据menu name 获取 value
     *
     * @param name menu name
     * @return String
     */
    @Override
    public String getValueByKey(String name) {
        return get(name);
    }

    /**
     * 根据 name 获取 value 找不到抛异常
     *
     * @param name menu name
     * @return String
     */
    @Override
    public String getValueByKeyException(String name) {
        String value = get(name);
        if (value == null) {
            throw new CrmebException("没有找到" + name + "数据");
        }
        return value;
    }

    /**
     * 把数据同步到redis
     *
     * @param name String
     * @return String
     */
    private String get(String name) {
        if (!crmebConfig.isAsyncConfig()) {
            // 如果配置没有开启
            LambdaQueryWrapper<SystemConfig> lambdaQueryWrapper = new LambdaQueryWrapper<>();
            lambdaQueryWrapper.eq(SystemConfig::getStatus, false).eq(SystemConfig::getName, name);
            SystemConfig systemConfig = dao.selectOne(lambdaQueryWrapper);
            if (ObjectUtil.isNull(systemConfig) || StrUtil.isBlank(systemConfig.getValue())) {
                return Constants.STR_EMPTY;
            }
            return systemConfig.getValue();
        }
        setRedisByVoList();
        Object data = redisUtil.hmGet(redisKey, name);
        if (ObjectUtil.isNull(data) || StrUtil.isBlank(data.toString())) {
            // 没有找到数据
            return Constants.STR_EMPTY;
        }
        // 去数据库查找，然后写入 redis
        return data.toString();
    }

    /**
     * 把数据同步到redis, 此方法适用于redis为空的时候进行一次批量输入
     */
    private void setRedisByVoList() {
        // 检测 redis 是否为空
        Long size = redisUtil.getHashSize(redisKey);
        if (size > 0 || !crmebConfig.isAsyncConfig()) return;

        LambdaQueryWrapper<SystemConfig> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(SystemConfig::getStatus, false);
        List<SystemConfig> systemConfigList = dao.selectList(lambdaQueryWrapper);
        async(systemConfigList);
    }

    /**
     * 把数据同步到redis
     *
     * @param systemConfigList List<SystemConfig> 需要同步的数据
     */
    private void async(List<SystemConfig> systemConfigList) {
        // 如果配置没有开启直接返回
        if (!crmebConfig.isAsyncConfig()) return;

        for (SystemConfig systemConfig : systemConfigList) {
            redisUtil.hmSet(redisKey, systemConfig.getName(), systemConfig.getValue());
        }

    }
}
