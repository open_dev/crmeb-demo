package com.ncamc.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ncamc.common.model.seckill.StoreSeckill;

import java.util.List;

public interface StoreSeckillService extends IService<StoreSeckill> {

    /**
     * 根据商品id查询正在秒杀的商品信息
     *
     * @param productId 商品id
     * @return 正在参与的秒杀信息
     */
    List<StoreSeckill> getCurrentSecKillByProductId(Integer productId);
}
