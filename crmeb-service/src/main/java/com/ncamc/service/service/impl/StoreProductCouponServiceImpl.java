package com.ncamc.service.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ncamc.common.model.product.StoreProductCoupon;
import com.ncamc.service.dao.StoreProductCouponDao;
import com.ncamc.service.service.StoreProductCouponService;
import org.springframework.stereotype.Service;

/**
 * StoreProductCouponServiceImpl 接口实现
 *
 * @Author : hugaoqiang 2023-11-10
 */
@Service
public class StoreProductCouponServiceImpl extends ServiceImpl<StoreProductCouponDao, StoreProductCoupon> implements StoreProductCouponService {
}
