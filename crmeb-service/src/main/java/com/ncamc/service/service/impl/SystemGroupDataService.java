package com.ncamc.service.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.ncamc.common.model.system.SystemGroupData;
import com.ncamc.common.request.PageParamRequest;
import com.ncamc.common.request.SystemFormItemCheckRequest;
import com.ncamc.common.request.SystemGroupDataSearchRequest;
import com.ncamc.common.utils.CrmebUtil;
import com.ncamc.service.dao.SystemGroupDataDao;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * SystemGroupDataServiceImpl 接口实现
 */
@Service
public class SystemGroupDataService extends ServiceImpl<SystemGroupDataDao, SystemGroupData> implements com.ncamc.service.service.SystemGroupDataService {

    @Resource
    private SystemGroupDataDao dao;

    /**
     * 分页组合数据详情
     *
     * @param request          搜索条件
     * @param pageParamRequest 分页参数
     */
    @Override
    public List<SystemGroupData> getList(SystemGroupDataSearchRequest request, PageParamRequest pageParamRequest) {
        PageHelper.startPage(pageParamRequest.getPage(), pageParamRequest.getLimit());

        // 带 SystemGroupData 类的多条件查询
        LambdaQueryWrapper<SystemGroupData> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        SystemGroupData mode = new SystemGroupData();
        BeanUtils.copyProperties(request, mode);
        lambdaQueryWrapper.setEntity(mode);
        lambdaQueryWrapper.orderByAsc(SystemGroupData::getSort).orderByAsc(SystemGroupData::getId);
        return dao.selectList(lambdaQueryWrapper);
    }

    /**
     * 通过gid获取列表 推荐二开使用
     *
     * @param gid Integer group id
     * @return List<T>
     */
    @Override
    public <T> List<T> getListByGid(Integer gid, Class<T> cls) {
        SystemGroupDataSearchRequest systemGroupDataSearchRequest = new SystemGroupDataSearchRequest();
        systemGroupDataSearchRequest.setGid(gid);
        systemGroupDataSearchRequest.setStatus(true);
        List<SystemGroupData> list = getList(systemGroupDataSearchRequest, new PageParamRequest());

        List<T> arrayList = new ArrayList<>();
        if (list.isEmpty()) return null;

        for (SystemGroupData systemGroupData : list) {
            JSONObject jsonObject = JSONObject.parseObject(systemGroupData.getValue());
            List<SystemFormItemCheckRequest> systemFormItemCheckRequestList = CrmebUtil.jsonToListClass(jsonObject.getString("fields"), SystemFormItemCheckRequest.class);
            if (systemFormItemCheckRequestList.isEmpty()) continue;

            HashMap<String, Object> map = new HashMap<>();
            T t;
            for (SystemFormItemCheckRequest systemFormItemCheckRequest : systemFormItemCheckRequestList) {
                map.put(systemFormItemCheckRequest.getName(), systemFormItemCheckRequest.getValue());
            }
            map.put("id", systemGroupData.getId());
            t = CrmebUtil.mapToObj(map, cls);
            arrayList.add(t);
        }
        return arrayList;
    }
}
