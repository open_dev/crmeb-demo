package com.ncamc.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ncamc.common.model.article.Article;

/**
 * ArticleService 接口
 */
public interface ArticleService extends IService<Article> {
}
