package com.ncamc.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ncamc.common.model.product.StoreProductAttrValue;

import java.util.List;

/**
 * StoreProductAttrValueService 接口
 */
public interface StoreProductAttrValueService extends IService<StoreProductAttrValue> {

    /**
     * 根据商品属性查询
     *
     * @param storeProductAttrValue 商品属性参数
     * @return 商品属性结果
     */
    List<StoreProductAttrValue> getByEntity(StoreProductAttrValue storeProductAttrValue);

}
