package com.ncamc.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ncamc.common.model.wechat.WechatReply;

public interface WechatReplyService extends IService<WechatReply> {

    /**
     * 根据关键字查询数据
     * @param keywords 新增参数
     * @return WechatReply
     */
    WechatReply getVoByKeywords(String keywords);

}
