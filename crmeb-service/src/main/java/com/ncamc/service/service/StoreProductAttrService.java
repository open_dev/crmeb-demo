package com.ncamc.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ncamc.common.model.product.StoreProductAttr;

import java.util.List;

/**
 * StoreProductAttrService 接口
 */
public interface StoreProductAttrService extends IService<StoreProductAttr> {

    /**
     * 根据基本属性查询商品属性详情
     *
     * @param storeProductAttr 商品属性
     * @return 查询商品属性集合
     */
    List<StoreProductAttr> getByEntity(StoreProductAttr storeProductAttr);

}
