package com.ncamc.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ncamc.common.model.system.SystemMenu;

import java.util.List;

/**
 * SystemMenuService 接口
 *
 * @Author : hugaoqiang 2023-09-22
 */
public interface SystemMenuService extends IService<SystemMenu> {

    /**
     * 获取所有菜单
     *
     * @return List<SystemMenu>
     */
    List<SystemMenu> findAllCatalogue();

    /**
     * 获取所有权限
     *
     * @return List
     */
    List<SystemMenu> getAllPermissions();

    /**
     * 通过用户id获取权限
     */
    List<SystemMenu> findPermissionByUserId(Integer userId);

    /**
     * 获取用户路由
     *
     * @param userId 用户id
     * @return List
     */
    List<SystemMenu> getMenusByUserId(Integer userId);
}
