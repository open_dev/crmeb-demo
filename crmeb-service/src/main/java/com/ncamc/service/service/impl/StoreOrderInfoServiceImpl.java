package com.ncamc.service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ncamc.common.model.order.StoreOrderInfo;
import com.ncamc.common.vo.StoreOrderInfoOldVo;
import com.ncamc.service.dao.StoreOrderInfoDao;
import com.ncamc.service.service.StoreOrderInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * StoreOrderInfoServiceImpl 接口实现
 *
 * @Author : hugaoqiang 2023-11-10
 */
@Service
public class StoreOrderInfoServiceImpl extends ServiceImpl<StoreOrderInfoDao, StoreOrderInfo> implements StoreOrderInfoService {

    @Resource
    private StoreOrderInfoDao dao;

    /**
     * 根据id集合查询数据，返回 map
     *
     * @param orderId Integer id
     * @return HashMap<Integer, StoreCart>
     */
    @Override
    public List<StoreOrderInfoOldVo> getOrderListByOrderId(int orderId) {
        LambdaQueryWrapper<StoreOrderInfo> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(StoreOrderInfo::getOrderId, orderId);
        List<StoreOrderInfo> systemStoreOrderInfoList = dao.selectList(lambdaQueryWrapper);
        if (systemStoreOrderInfoList.isEmpty()) {
            return null;
        }
        return null;
    }
}
