package com.ncamc.service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ncamc.common.model.seckill.StoreSeckill;
import com.ncamc.common.request.PageParamRequest;
import com.ncamc.common.response.StoreSeckillManagerResponse;
import com.ncamc.service.dao.StoreSeckillDao;
import com.ncamc.service.service.StoreSeckillMangerService;
import com.ncamc.service.service.StoreSeckillService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * StoreSeckillService 实现类
 *
 * @Author : hugaoqiang 2023-11-10
 */
@Service
public class StoreSeckillServiceImpl extends ServiceImpl<StoreSeckillDao, StoreSeckill> implements StoreSeckillService {

    private static final Logger logger = LoggerFactory.getLogger(StoreSeckillServiceImpl.class);

    @Resource
    private StoreSeckillDao dao;
    @Resource
    private StoreSeckillMangerService storeSeckillMangerService;

    /**
     * 根据商品id查询正在秒杀的商品信息
     *
     * @param productId 商品id
     * @return 正在参与的秒杀信息
     */
    @Override
    public List<StoreSeckill> getCurrentSecKillByProductId(Integer productId) {
        List<StoreSeckill> result = new ArrayList<>();
        // 获取当前时间段的秒杀
        PageParamRequest pageParamRequest = new PageParamRequest();
        pageParamRequest.setLimit(20);
        List<StoreSeckillManagerResponse> storeSeckillMangerRequests = storeSeckillMangerService.getAllList();
        List<StoreSeckillManagerResponse> currentSmr =
                storeSeckillMangerRequests.stream().filter(e -> e.getKillStatus() == 2).collect(Collectors.toList());
        if (currentSmr.isEmpty()) {
            return result;
        }
        List<Integer> skillManagerIds = currentSmr.stream().map(StoreSeckillManagerResponse::getId).collect(Collectors.toList());
        // 获取正在秒杀的商品信息
        LambdaQueryWrapper<StoreSeckill> lqw = Wrappers.lambdaQuery();
        lqw.eq(StoreSeckill::getProductId, productId);
        lqw.eq(StoreSeckill::getIsDel, false);
        lqw.eq(StoreSeckill::getSales, 1);
        lqw.eq(StoreSeckill::getTimeId, skillManagerIds);
        result = dao.selectList(lqw);
        return result;
    }
}
