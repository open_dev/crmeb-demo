package com.ncamc.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ncamc.common.model.product.StoreProductCoupon;

public interface StoreProductCouponService extends IService<StoreProductCoupon> {
}
