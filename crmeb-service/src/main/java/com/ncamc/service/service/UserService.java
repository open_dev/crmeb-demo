package com.ncamc.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ncamc.common.model.user.User;

import java.util.List;
import java.util.Map;

/**
 * 用户表 服务类
 *
 * @Author : hugaoqiang 2023-11-10
 */
public interface UserService extends IService<User> {

    Integer getUserIdException();

    /**
     * 根据用户id获取用户列表 map模式
     *
     * @param uidList uidList
     * @return HashMap
     */
    Map<Integer, User> getMapListInUid(List<Integer> uidList);

    /**
     * 根据日期获取注册用户数量
     *
     * @param date 日期
     * @return Integer
     */
    Integer getRegisterNumByDate(String date);

    /**
     * 按开始结束时间查询每日新增用户数量
     *
     * @param date String 时间范围
     * @return HashMap<String, Object>
     */
    Map<Object, Object> getAddUserCountGroupDate(String date);
}
