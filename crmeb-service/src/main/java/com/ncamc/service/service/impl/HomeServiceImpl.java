package com.ncamc.service.service.impl;

import com.ncamc.common.constants.Constants;
import com.ncamc.common.model.order.StoreOrder;
import com.ncamc.common.model.record.UserVisitRecord;
import com.ncamc.common.response.HomeRateResponse;
import com.ncamc.common.utils.DateUtil;
import com.ncamc.service.service.HomeService;
import com.ncamc.service.service.StoreOrderService;
import com.ncamc.service.service.UserService;
import com.ncamc.service.service.UserVisitRecordService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户表 服务实现类
 *
 * @Author : hugaoqiang 2023-11-15
 */
@Service
public class HomeServiceImpl implements HomeService {

    @Resource
    private StoreOrderService storeOrderService;

    @Resource
    private UserVisitRecordService userVisitRecordService;

    @Resource
    private UserService userService;

    /**
     * 用户曲线图
     */
    @Override
    public Map<Object, Object> chartUser() {
        return dataFormat(userService.getAddUserCountGroupDate(Constants.SEARCH_DATE_LATELY_30), Constants.SEARCH_DATE_LATELY_30);
    }

    /**
     * 订单量趋势
     */
    @Override
    public Map<String, Object> chartOrder() {
        Map<String, Object> map = new HashMap<>();

        // 按开始结束时间分组订单
        List<StoreOrder> list = storeOrderService.getOrderGroupByDate(Constants.SEARCH_DATE_LATELY_30, Constants.NUM_TEN);

        map.put("quality", dataFormat(getOrderCountGroupByDate(list), Constants.SEARCH_DATE_LATELY_30));
        map.put("price", dataFormat(getOrderPriceGroupByDate(list), Constants.SEARCH_DATE_LATELY_30));
        return map;
    }

    /**
     * 按开始结束时间查询每日新增订单数量
     *
     * @param list List<StoreOrder> 时间范围
     * @return HashMap<String, Object>
     */
    private Map<Object, Object> getOrderCountGroupByDate(List<StoreOrder> list) {
        Map<Object, Object> map = new HashMap<>();

        if (list.isEmpty()) {
            return map;
        }

        for (StoreOrder storeOrder : list) {
            map.put(storeOrder.getOrderId(), storeOrder.getId());
        }

        return map;
    }

    /**
     * 按开始结束时间查询每日新增订单销售额
     *
     * @param list List<StoreOrder> 时间范围
     * @return HashMap<String, Object>
     */
    private Map<Object, Object> getOrderPriceGroupByDate(List<StoreOrder> list) {
        Map<Object, Object> map = new HashMap<>();

        if (list.isEmpty()) {
            return map;
        }

        for (StoreOrder storeOrder : list) {
            map.put(storeOrder.getOrderId(), storeOrder.getPayPrice());
        }

        return map;
    }

    /**
     * 日期和数量格式化
     *
     * @return Map<String, Integer>
     */
    private Map<Object, Object> dataFormat(Map<Object, Object> countGroupDate, String dateLimit) {
        Map<Object, Object> map = new LinkedHashMap<>();
        List<String> listDate = DateUtil.getListDate(dateLimit);

        String[] weekList = new String[]{"周一", "周二", "周三", "周四", "周五", "周六", "周日"};

        int i = 0;
        for (String date : listDate) {
            Object count = 0;
            if (countGroupDate.containsKey(date)) {
                count = countGroupDate.get(date);
            }
            String key;

            // 周格式化
            switch (dateLimit) {
                // 格式化周
                case Constants.SEARCH_DATE_WEEK:
                case Constants.SEARCH_DATE_PRE_WEEK:
                    key = weekList[i];
                    break;
                // 格式化月
                case Constants.SEARCH_DATE_MONTH:
                case Constants.SEARCH_DATE_PRE_MONTH:
                    key = i + 1 + Constants.STR_EMPTY;
                    break;
                // 默认显示两位日期
                default:
                    key = date.substring(5, 10);
            }
            map.put(key, count);
            i++;
        }
        return map;
    }

    /**
     * 首页数据
     * 今日/昨日
     * 销售额
     * 用户访问量
     * 订单量
     * 新增用户
     * @return HomeRateResponse
     */
    @Override
    public HomeRateResponse indexDate() {
        String today = cn.hutool.core.date.DateUtil.date().toString("yyyy-MM-dd");
        String yesterday = cn.hutool.core.date.DateUtil.yesterday().toString("yyyy-MM-dd");
        HomeRateResponse response = new HomeRateResponse();
        // 通过日期获取支付订单金额
        response.setSales(storeOrderService.getPayOrderAmountByDate(today));
        response.setYesterdaySales(storeOrderService.getPayOrderAmountByDate(yesterday));
        // 通过日期获取浏览量
        response.setPageviews(userVisitRecordService.getPageviewsByDate(today));
        response.setYesterdayPageviews(userVisitRecordService.getPageviewsByDate(yesterday));
        // 通过日期获取订单数量
        response.setOrderNum(storeOrderService.getOrderNumByDate(today));
        response.setYesterdayOrderNum(storeOrderService.getOrderNumByDate(yesterday));
        // 根据日期获取注册用户数量
        response.setNewUserNum(userService.getRegisterNumByDate(today));
        response.setYesterdayNewUserNum(userService.getRegisterNumByDate(yesterday));
        return response;
    }
}
