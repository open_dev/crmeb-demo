package com.ncamc.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ncamc.common.model.system.SystemAdmin;

/**
 * SystemAdminService 接口
 */
public interface SystemAdminService extends IService<SystemAdmin> {

    /**
     * 通过用户名获取用户
     *
     * @param username 用户名
     * @return SystemAdmin
     */
    SystemAdmin selectUserByUserName(String username);

}
