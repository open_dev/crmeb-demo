package com.ncamc.service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ncamc.common.model.product.StoreProductAttrValue;
import com.ncamc.service.dao.StoreProductAttrValueDao;
import com.ncamc.service.service.StoreProductAttrValueService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * StoreProductAttrValueServiceImpl 接口实现
 *
 * @Author : hugaoqiang 2023-11-16
 */
@Service
public class StoreProductAttrValueServiceImpl extends ServiceImpl<StoreProductAttrValueDao, StoreProductAttrValue> implements StoreProductAttrValueService {

    @Resource
    private StoreProductAttrValueDao dao;

    /**
     * 根据产品属性查询
     *
     * @param storeProductAttrValue 商品属性参数
     * @return 查询到的属性结果
     */
    @Override
    public List<StoreProductAttrValue> getByEntity(StoreProductAttrValue storeProductAttrValue) {
        LambdaQueryWrapper<StoreProductAttrValue> lqw = new LambdaQueryWrapper<>();
        lqw.setEntity(storeProductAttrValue);
        return dao.selectList(lqw);
    }
}
