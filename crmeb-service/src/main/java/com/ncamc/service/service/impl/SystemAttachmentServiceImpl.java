package com.ncamc.service.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ncamc.common.constants.Constants;
import com.ncamc.common.model.system.SystemAttachment;
import com.ncamc.service.dao.SystemAttachmentDao;
import com.ncamc.service.service.SystemAttachmentService;
import com.ncamc.service.service.SystemConfigService;
import jdk.nashorn.internal.ir.annotations.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * SystemAttachmentServiceImpl 接口实现
 *
 * @Author : hugaoqiang 2023-11-13
 */
@Service
public class SystemAttachmentServiceImpl extends ServiceImpl<SystemAttachmentDao, SystemAttachment> implements SystemAttachmentService {

    @Resource
    private SystemAttachmentDao dao;

    @Resource
    private SystemConfigService systemConfigService;

    /**
     * 给图片加前缀
     *
     * @param path String 路径
     * @return String
     */
    @Override
    public String prefixImage(String path) {
        String cdnUrl = getCdnUrl();
        // 如果那些域名不需要加，则跳过
        return path.replace(Constants.UPLOAD_TYPE_IMAGE + "/", cdnUrl + "/" + Constants.UPLOAD_TYPE_IMAGE + "/");
    }

    /**
     * 给文件加前缀
     *
     * @param path String 路径
     * @return String
     */
    @Override
    public String prefixFile(String path) {
        if (path.contains("file/excel")) {
            String cdnUrl = systemConfigService.getValueByKey("local" + "UploadUrl");
            return path.replace("file/", cdnUrl + "/file/");
        }
        return path.replace("file/", getCdnUrl() + "/file/");
    }

    /**
     * 获取 cdn url
     *
     * @return String
     */
    private String getCdnUrl() {
        String uploadType = systemConfigService.getValueByKeyException("uploadType");
        // 获取配置信息
        int type = Integer.parseInt(uploadType);
        String pre = "local";
        switch (type) {
            case 2:
                pre = "qn";
                break;
            case 3:
                pre = "al";
                break;
            case 4:
                pre = "tx";
                break;
            default:
                break;
        }
        return systemConfigService.getValueByKey(pre + "UploadUrl");
    }
}
