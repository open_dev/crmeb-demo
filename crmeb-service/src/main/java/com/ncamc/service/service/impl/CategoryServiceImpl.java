package com.ncamc.service.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ncamc.common.model.category.Category;
import com.ncamc.common.vo.CategoryTreeVo;
import com.ncamc.service.dao.CategoryDao;
import com.ncamc.service.service.CategoryService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * CategoryServiceImpl 接口实现
 *
 * @Author : hugaoqiang 2023-11-16
 */
@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, Category> implements CategoryService {

    @Resource
    private CategoryDao dao;

    /**
     * 通过id集合获取列表
     *
     * @param idList List<Integer> id集合
     * @return List<Category>
     */
    @Override
    public List<Category> getByIds(List<Integer> idList) {
        LambdaQueryWrapper<Category> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.in(Category::getId, idList);
        return dao.selectList(lambdaQueryWrapper);
    }

    /**
     * 获取树形结构数据
     *
     * @param type   分类
     * @param status 状态
     * @param name   名称
     * @return List
     */
    @Override
    public List<CategoryTreeVo> getListTree(Integer type, Integer status, String name) {
        return getTree(type, status, name, null);
    }

    /**
     * 带结构的无线级分类
     */
    private List<CategoryTreeVo> getTree(Integer type, Integer status, String name, List<Integer> categoryIdList) {
        // 循环数据，把数据对象变成带 list 结构的 vo
        List<CategoryTreeVo> treeList = new ArrayList<>();

        LambdaQueryWrapper<Category> lambdaQueryWrapper = Wrappers.lambdaQuery();
        lambdaQueryWrapper.eq(Category::getType, type);

        if (null != categoryIdList && !categoryIdList.isEmpty()) {
            lambdaQueryWrapper.in(Category::getId, categoryIdList);
        }

        if (status >= 0) {
            lambdaQueryWrapper.eq(Category::getStatus, status);
        }

        if (StringUtils.isNotBlank(name)) { // 根据名称模糊搜索
            lambdaQueryWrapper.like(Category::getName, name);
        }

        lambdaQueryWrapper.orderByDesc(Category::getSort);
        lambdaQueryWrapper.orderByAsc(Category::getId);
        List<Category> allTree = dao.selectList(lambdaQueryWrapper);

        if (allTree == null) {
            return null;
        }

        // 根据名称搜索特殊处理 这里仅仅处理两层搜索后有子父级关系的数据
        if (StringUtils.isNotBlank(name) && !allTree.isEmpty()) {
            List<Category> searchCategory = new ArrayList<>();
            List<Integer> categoryIds = allTree.stream().map(Category::getId).collect(Collectors.toList());
            List<Integer> pidList = allTree.stream().filter(c -> c.getPid() > 0 && !categoryIds.contains(c.getPid()))
                    .map(Category::getPid).distinct().collect(Collectors.toList());

            if (CollUtil.isNotEmpty(pidList)) {
                pidList.forEach(pid -> {
                    searchCategory.add(dao.selectById(pid));
                });
            }
            allTree.addAll(searchCategory);
        }

        for (Category category : allTree) {
            CategoryTreeVo categoryTreeVo = new CategoryTreeVo();
            BeanUtils.copyProperties(category, categoryTreeVo);
            treeList.add(categoryTreeVo);
        }

        // 返回
        Map<Integer, CategoryTreeVo> map = new HashMap<>();
        // ID 为 key 存储到 map 中
        for (CategoryTreeVo categoryTreeVo : treeList) {
            map.put(categoryTreeVo.getId(), categoryTreeVo);
        }

        List<CategoryTreeVo> list = new ArrayList<>();
        for (CategoryTreeVo tree : treeList) {
            // 子集 ID 返回对象，有则添加
            CategoryTreeVo categoryTreeVo = map.get(tree.getPid());
            if (categoryTreeVo != null) {
                categoryTreeVo.getChild().add(tree);
            } else {
                list.add(tree);
            }
        }
        System.out.println("无限极分类 : getTree:" + JSON.toJSONString(list));
        return list;
    }

}
