package com.ncamc.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ncamc.common.model.system.SystemStore;

import java.util.List;
import java.util.Map;

/**
 * SystemStoreService 接口
 */
public interface SystemStoreService extends IService<SystemStore> {

    Map<Integer, SystemStore> getMaoInId(List<Integer> storeIdList);

}
