package com.ncamc.admin.controller;

import com.ncamc.common.response.CommonResult;
import com.ncamc.service.service.SystemConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 配置表 前端控制器
 *
 * @Author : hugaoqiang 2023-11-15
 */
@Slf4j
@RestController
@RequestMapping("api/admin/system/config")
@Api(tags = "设置 -- Config")
public class SystemConfigController {

    @Resource
    private SystemConfigService systemConfigService;

    /**
     * 根据key获取表单配置数据
     *
     * @param key 配置表的的字段
     */
    @PreAuthorize("hasAuthority('admin:system:config:getuniq')")
    @ApiOperation(value = "表单配置根据 key 获取")
    @RequestMapping(value = "/getuniq", method = RequestMethod.GET)
    public CommonResult<Object> justGetUniq(@RequestParam String key) {
        return CommonResult.success(systemConfigService.getValueByKey(key), "success");
    }

}
