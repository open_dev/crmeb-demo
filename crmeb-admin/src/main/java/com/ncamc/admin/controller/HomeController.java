package com.ncamc.admin.controller;

import com.ncamc.common.response.CommonResult;
import com.ncamc.common.response.HomeRateResponse;
import com.ncamc.service.service.HomeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 统计 -- 主页 前端控制器
 *
 * @Author : hugaoqiang 2023-11-15
 */
@Slf4j
@RestController
@RequestMapping("api/admin/statistics/home")
@Api(tags = "统计 -- 主页")
public class HomeController {

    @Resource
    private HomeService homeService;

    @PreAuthorize("hasAuthority('admin:statistics:home:index')")
    @ApiOperation(value = "首页数据")
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public CommonResult<HomeRateResponse> indexDate() {
        return CommonResult.success(homeService.indexDate());
    }

    /**
     * 用户曲线图
     */
    @PreAuthorize("hasAuthority('admin:statistics:home:chart:user')")
    @ApiOperation(value = "用户曲线图")
    @RequestMapping(value = "/chart/user", method = RequestMethod.GET)
    public CommonResult<Map<Object, Object>> chartUser() {
        return CommonResult.success(homeService.chartUser());
    }

    /**
     * 30天订单量趋势
     */
    @PreAuthorize("hasAuthority('admin:statistics:home:chart:order')")
    @ApiOperation(value = "30天订单量趋势")
    @RequestMapping(value = "/chart/order", method = RequestMethod.GET)
    public CommonResult<Map<String, Object>> chartOrder() {
        return CommonResult.success(homeService.chartOrder());
    }

}
