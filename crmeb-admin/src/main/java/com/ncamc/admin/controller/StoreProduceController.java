package com.ncamc.admin.controller;

import com.ncamc.common.page.CommonPage;
import com.ncamc.common.request.PageParamRequest;
import com.ncamc.common.request.StoreProductSearchRequest;
import com.ncamc.common.response.CommonResult;
import com.ncamc.common.response.StoreProductResponse;
import com.ncamc.common.response.StoreProductTabsHeader;
import com.ncamc.service.service.StoreProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 商品表 前端控制器
 *
 * @Author : hugaoqiang 2023-11-15
 */
@Slf4j
@RestController
@RequestMapping("api/admin/store/product")
@Api(tags = "商品") //配合swagger使用
public class StoreProduceController {

    @Resource
    private StoreProductService storeProductService;

    /**
     * 分页显示商品表
     *
     * @param request          搜索条件
     * @param pageParamRequest 分页参数
     */
    @PreAuthorize("hasAuthority('admin:product:list')")
    @ApiOperation(value = "分页列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public CommonResult<CommonPage<StoreProductResponse>> getList(
            @Validated StoreProductSearchRequest request,
            @Validated PageParamRequest pageParamRequest) {
        return CommonResult.success(CommonPage.restPage(storeProductService.getAdminList(request, pageParamRequest)));
    }

    /**
     * 商品tabs表头数据
     */
    @PreAuthorize("hasAuthority('admin:product:tabs:headers')")
    @ApiOperation(value = "商品表头数量")
    @RequestMapping(value = "/tabs/headers", method = RequestMethod.GET)
    public CommonResult<List<StoreProductTabsHeader>> getTabsHeader() {
        return CommonResult.success(storeProductService.getTabsHeader());
    }

}
