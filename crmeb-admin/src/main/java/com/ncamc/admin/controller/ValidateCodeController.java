package com.ncamc.admin.controller;

import com.ncamc.admin.service.ValidateCodeService;
import com.ncamc.common.response.CommonResult;
import com.ncamc.admin.vo.ValidateCode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 验证码服务
 *
 * @Author : hugaoqiang 2023-09-25
 */
@Slf4j
@RestController
@RequestMapping("api/admin/validate/code")
@Api(tags = "验证码服务")
public class ValidateCodeController {

    @Resource
    private ValidateCodeService validateCodeService;

    /**
     * 获取图片验证码
     *
     * @return CommonResult
     */
    @ApiOperation(value = "获取验证码")
    @GetMapping(value = "/get")
    public CommonResult<ValidateCode> get() {
        ValidateCode validateCode = validateCodeService.get();
        return CommonResult.success(validateCode);
    }
}
