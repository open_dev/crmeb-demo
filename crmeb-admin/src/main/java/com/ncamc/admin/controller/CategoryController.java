package com.ncamc.admin.controller;

import com.ncamc.common.response.CommonResult;
import com.ncamc.common.vo.CategoryTreeVo;
import com.ncamc.service.service.CategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 分类表 前端控制器
 *
 * @Author : hugaoqiang 2023-11-17
 */
@Slf4j
@RestController
@RequestMapping("api/admin/category")
@Api(tags = "分类服务")
public class CategoryController {

    @Resource
    private CategoryService categoryService;

    /**
     * 查询分类表信息
     */
    @PreAuthorize("hasAuthority('admin:category:list:tree')")
    @ApiOperation(value = "获取tree结构的列表")
    @RequestMapping(value = "/list/tree", method = RequestMethod.GET)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type", value = "类型ID | 类型，1 产品分类，2 附件分类，3 文章分类， 4 设置分类， 5 菜单分类， 6 配置分类， 7 秒杀配置", example = "1"),
            @ApiImplicitParam(name = "status", value = "-1=全部，0=未生效，1=已生效", example = "1"),
            @ApiImplicitParam(name = "name", value = "模糊搜索", example = "电视")
    })
    public CommonResult<List<CategoryTreeVo>> getListTree(@RequestParam(name = "type") Integer type,
                                                          @RequestParam(name = "status") Integer status,
                                                          @RequestParam(name = "name", required = false) String name) {
        List<CategoryTreeVo> listTree = categoryService.getListTree(type, status, name);
        return CommonResult.success(listTree);
    }

}
