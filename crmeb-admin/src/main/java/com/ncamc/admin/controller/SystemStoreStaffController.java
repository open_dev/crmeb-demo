package com.ncamc.admin.controller;

import com.ncamc.admin.service.SystemStoreStaffService;
import com.ncamc.common.page.CommonPage;
import com.ncamc.common.request.PageParamRequest;
import com.ncamc.common.response.CommonResult;
import com.ncamc.common.response.SystemStoreStaffResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 门店店员表 前端控制器
 *
 * @Author : hugaoqiang 2023-11-14
 */
@Slf4j
@RestController
@RequestMapping("api/admin/system/store/staff")
@Api(tags = "设置 -- 提货点 -- 核销员")
public class SystemStoreStaffController {

    @Resource
    private SystemStoreStaffService systemStoreStaffService;

    /**
     * 分页显示门店核销员列表
     *
     * @param storeId          门店id
     * @param pageParamRequest 分页参数
     */
    @PreAuthorize("hasAuthority('admin:system:staff:list')")
    @ApiOperation(value = "分页列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public CommonResult<CommonPage<SystemStoreStaffResponse>> getList(@RequestParam(name = "storeId", required = false, defaultValue = "0") Integer storeId,
                                                                      @ModelAttribute PageParamRequest pageParamRequest) {
        CommonPage<SystemStoreStaffResponse> systemStoreStaffCommonPage =
                CommonPage.restPage(systemStoreStaffService.getList(storeId, pageParamRequest));
        return CommonResult.success(systemStoreStaffCommonPage);
    }

}
