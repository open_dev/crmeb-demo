package com.ncamc.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.ncamc.common.model.system.SystemStoreStaff;
import com.ncamc.common.request.PageParamRequest;
import com.ncamc.common.response.SystemStoreStaffResponse;

/**
 * SystemStoreStaffService 接口
 */
public interface SystemStoreStaffService extends IService<SystemStoreStaff> {

    /**
     * 分页显示门店核销员列表
     *
     * @param storeId          门店id
     * @param pageParamRequest 分页参数
     */
    PageInfo<SystemStoreStaffResponse> getList(Integer storeId, PageParamRequest pageParamRequest);

}
