package com.ncamc.admin.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ncamc.admin.service.SystemStoreStaffService;
import com.ncamc.common.model.system.SystemStore;
import com.ncamc.common.model.system.SystemStoreStaff;
import com.ncamc.common.model.user.User;
import com.ncamc.common.page.CommonPage;
import com.ncamc.common.request.PageParamRequest;
import com.ncamc.common.response.SystemStoreStaffResponse;
import com.ncamc.service.dao.SystemStoreStaffDao;
import com.ncamc.service.service.SystemStoreService;
import com.ncamc.service.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * SystemStoreStaffServiceImpl 接口实现
 *
 * @Author : hugaoqiang 2023-11-14
 */
@Service
public class SystemStoreStaffServiceImpl extends ServiceImpl<SystemStoreStaffDao, SystemStoreStaff> implements SystemStoreStaffService {

    @Resource
    private SystemStoreStaffDao dao;

    @Resource
    private UserService userService;

    @Resource
    private SystemStoreService systemStoreService;

    /**
     * 分页显示门店核销员列表
     *
     * @param storeId          门店id
     * @param pageParamRequest 分页参数
     */
    @Override
    public PageInfo<SystemStoreStaffResponse> getList(Integer storeId, PageParamRequest pageParamRequest) {
        Page<Object> systemStorePage = PageHelper.startPage(pageParamRequest.getPage(), pageParamRequest.getLimit());
        LambdaQueryWrapper<SystemStoreStaff> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        if (storeId > 0) {
            lambdaQueryWrapper.eq(SystemStoreStaff::getStoreId, storeId);
        }
        List<SystemStoreStaffResponse> systemStoreStaffResponseList = new ArrayList<>();
        List<SystemStoreStaff> systemStoreStaffList = dao.selectList(lambdaQueryWrapper);
        if (CollUtil.isEmpty(systemStoreStaffList)) {
            return new PageInfo<>();
        }

        // 用户信息
        List<Integer> userIdList = systemStoreStaffList.stream().map(SystemStoreStaff::getUid).collect(Collectors.toList());
        Map<Integer, User> userList = null;
        if (userIdList.isEmpty()) {
            userList = userService.getMapListInUid(userIdList);
        }

        // 门店信息
        List<Integer> storeIdList = systemStoreStaffList.stream().map(SystemStoreStaff::getStoreId).collect(Collectors.toList());
        Map<Integer, SystemStore> storeList = null;
        if (storeIdList.isEmpty()) {
            storeList = systemStoreService.getMaoInId(storeIdList);
        }

        for (SystemStoreStaff systemStoreStaff : systemStoreStaffList) {
            SystemStoreStaffResponse systemStoreStaffResponse = new SystemStoreStaffResponse();
            BeanUtils.copyProperties(systemStoreStaff, systemStoreStaffResponse);
            if (CollUtil.isNotEmpty(userList) && userList.containsKey(systemStoreStaff.getUid())) {
                systemStoreStaffResponse.setUser(userList.get(systemStoreStaff.getUid()));
            }
            if (CollUtil.isNotEmpty(storeList) && storeList.containsKey(systemStoreStaff.getStoreId())) {
                systemStoreStaffResponse.setSystemStore(storeList.get(systemStoreStaff.getStoreId()));
            }
            systemStoreStaffResponseList.add(systemStoreStaffResponse);
        }
        return CommonPage.copyPageInfo(systemStorePage, systemStoreStaffResponseList);
    }
}
