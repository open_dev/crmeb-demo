package com.ncamc.admin.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.ncamc.common.constants.Constants;
import com.ncamc.common.exception.CrmebException;
import com.ncamc.common.utils.CrmebUtil;
import com.ncamc.common.utils.RedisUtil;
import com.ncamc.common.utils.ValidateCodeUtil;
import com.ncamc.admin.vo.ValidateCode;
import com.ncamc.admin.service.ValidateCodeService;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * @Author : hugaoqiang 2023-09-25
 */
@Service
public class ValidateCodeServiceImpl implements ValidateCodeService {

    @Resource
    private RedisUtil redisAdminUtil;

    /**
     * 获取图片验证码
     */
    @Override
    public ValidateCode get() {
        // 直接调用静态方法，返回验证码对象
        ValidateCodeUtil.Validate randomCode = ValidateCodeUtil.getRandomCode();
        if (ObjectUtil.isNull(randomCode)) return null;

        // 将验证码转换为小写
        String value = randomCode.getValue().toLowerCase();
        // 使用 md5 加密 value
        String md5Hex = DigestUtils.md5Hex(value);
        // 获取 redis key
        String redisKey = getRedisKey(md5Hex);
        // 设置过期时间 5 分钟后国企
        redisAdminUtil.set(redisKey, value, 5L, TimeUnit.MINUTES);
        String base64Str = randomCode.getBase64Str();
        return new ValidateCode(md5Hex, CrmebUtil.getBase64Image(base64Str));
    }

    /**
     * 获取redis key
     *
     * @param md5Key value的md5加密值
     */
    public String getRedisKey(String md5Key) {
        return Constants.VALIDATE_REDIS_KEY_PREFIX + md5Key;
    }

    /**
     * 验证
     */
    public Boolean check(String key, String code) {
        if (!redisAdminUtil.exists(getRedisKey(key))) throw new CrmebException("验证码错误");

        Object redisValue = redisAdminUtil.get(getRedisKey(key));
        if (ObjectUtil.isNull(redisValue)) return false;

        return redisValue.equals(code.toLowerCase());
    }
}
