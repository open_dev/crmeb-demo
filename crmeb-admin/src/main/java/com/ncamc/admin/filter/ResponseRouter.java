package com.ncamc.admin.filter;

import com.ncamc.common.constants.Constants;
import com.ncamc.common.utils.SpringUtil;
import com.ncamc.service.service.SystemAttachmentService;

/**
 * response路径处理
 */
public class ResponseRouter {

    public static String un() {
        return Constants.STR_EMPTY;
    }

    public String filter(String data, String path) {
        boolean result = un().contains(path);
        if (result) {
            return data;
        }

        //根据需要处理返回值
        if (data.contains(Constants.UPLOAD_TYPE_IMAGE + "/") && !data.contains("data:image/png;base64")) {
            data = SpringUtil.getBean(SystemAttachmentService.class).prefixImage(data);
        }

        return data;
    }
}
