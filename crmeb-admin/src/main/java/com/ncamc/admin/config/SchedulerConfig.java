package com.ncamc.admin.config;

import lombok.Data;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

/**
 * Spring 定时任务配置
 *
 * @Author : hugaoqiang 2023-09-22
 */
@Data
@Configuration
public class SchedulerConfig implements SchedulingConfigurer {

    private final int poolSize = 30;

    @Override
    public void configureTasks(ScheduledTaskRegistrar scheduledTaskRegistrar) {
        ThreadPoolTaskScheduler threadPoolTaskScheduler = new ThreadPoolTaskScheduler();
        // 设置池大小
        threadPoolTaskScheduler.setPoolSize(getPoolSize());
        // 设置线程名称前缀
        threadPoolTaskScheduler.setThreadNamePrefix("crmeb-scheduled-task-pool-");
        // 初始化
        threadPoolTaskScheduler.initialize();
        scheduledTaskRegistrar.setTaskScheduler(threadPoolTaskScheduler);
    }
}
