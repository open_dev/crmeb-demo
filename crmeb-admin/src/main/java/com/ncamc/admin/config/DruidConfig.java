package com.ncamc.admin.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * Druid配置组件
 *
 * @Author : hugaoqiang 2023-09-22
 */
@Configuration
public class DruidConfig {

    /**
     * Servlet 注册 Bean
     */
    @Bean
    public ServletRegistrationBean druidServlet() {// 主要实现WEB监控的配置处理
        // 进行druid监控的配置处理操作
        ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(new StatViewServlet(), "/druid/*");
        servletRegistrationBean.addInitParameter("loginUsername", "kf"); // 用户名
        servletRegistrationBean.addInitParameter("loginPassword", "654321"); // 密码
        servletRegistrationBean.addInitParameter("resetEnable", "true"); // 是否可以重置数据源
        return servletRegistrationBean;
    }

    /**
     * Filter 注册 Bean
     */
    @Bean
    public FilterRegistrationBean filterRegistrationBean() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        filterRegistrationBean.setFilter(new WebStatFilter()); // 设置过滤器
        filterRegistrationBean.addUrlPatterns("/*"); // 所有请求进行监控处理
        //不必监控的请求
        filterRegistrationBean.addInitParameter("exclusions", "*.html,*.png,*.ico,*.js,*.gif,*.jpg,*.css,/druid/*");
        return filterRegistrationBean;
    }

    /**
     * 配置数据源
     */
    @Bean("dataSource") // 标明 BeanName
    @ConfigurationProperties(prefix = "spring.datasource") // 从配置文件中获取
    public DataSource druidDataSource() {
        return new DruidDataSource();
    }
}
