package com.ncamc.common.request;

import lombok.Data;

/**
 * 订单时间参数，开始时间结束时间
 */
@Data
public class StoreDateRangeSqlPram {

    private String startTime;
    private String endTime;
    public StoreDateRangeSqlPram(String startTime, String endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }
}
