package com.ncamc.common.response;

import com.ncamc.common.model.bargain.StoreBargain;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;

/**
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "BargainIndexResponse对象", description = "砍价首页响应对象")
public class BargainIndexResponse {

    @ApiModelProperty(value = "拼团商品列表")
    private List<StoreBargain> productList;

}
