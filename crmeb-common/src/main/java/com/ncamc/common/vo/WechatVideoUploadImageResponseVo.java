package com.ncamc.common.vo;

import lombok.Data;

/**
 * 微信视频上传图片响应Vo对象
 */
@Data
public class WechatVideoUploadImageResponseVo extends BaseResultResponseVo {

    private imageInfo img_info;

    @Data
    class imageInfo {
        private String media_id;
    }
}
