package com.ncamc.common.vo;

import lombok.Data;

/**
 * 注册检查访问信息VO对象
 */
@Data
public class RegisterCheckAccessInfoItemVo {

    // 上传商品并审核成功
    private Integer spu_audit_success;

    // 发起第一笔订单并支付成功
    private Integer pay_order_success;
}
