package com.ncamc.common.vo;

import lombok.Data;

/**
 * 时间Vo类
 */
@Data
public class dateLimitUtilVo {
    private String startTime; //开始时间
    private String endTime; //结束时间

    public dateLimitUtilVo() {
    }

    public dateLimitUtilVo(String startTime, String endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }
}
