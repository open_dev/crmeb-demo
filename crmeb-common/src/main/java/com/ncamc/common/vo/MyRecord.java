package com.ncamc.common.vo;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.*;

/**
 * 公共对象
 *
 * @Author : hugaoqiang 2023-09-22
 */
public class MyRecord implements Serializable {

    private static final long serialVersionUID = 905784513600884082L;

    private static final Logger logger = LoggerFactory.getLogger(MyRecord.class);

    private Map<String, Object> columns;

    /**
     * @param o
     * @desc 获取属性名数组
     */
    private static String[] getFileName(Object o) {
        Field[] fields = o.getClass().getDeclaredFields();
        String[] fieldName = new String[fields.length];
        for (int i = 0; i < fields.length; i++) {
            fieldName[i] = fields[i].getName();
        }
        return fieldName;
    }

    /**
     * @param fieldName
     * @param o
     * @desc 根据属性名获取属性值
     */
    private static Object getFieldValueByName(String fieldName, Object o) {
        try {
            String firstLetter = fieldName.substring(0, 1).toUpperCase();
            String getter = "get" + firstLetter + fieldName.substring(1);
            Method method = o.getClass().getMethod(getter, new Class[]{});
            return method.invoke(o, new Object[]{});
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("获取属性直失败！{}", e, e);
        }
        return null;
    }

    /**
     * @param fieldName
     * @param o
     * @desc 获取属性的数据类型
     */
    private static Object getFiledType(String fieldName, Object o) {
        Field[] fields = o.getClass().getDeclaredFields(); // 获取声明字段
        for (Field field : fields) {
            if (Objects.equals(fieldName, field.getName())) {
                return field.getType();
            }
        }
        return null;
    }

    void SetColumnsMap(Map<String, Object> columns) {
        this.columns = columns;
    }

    /**
     * 返回列映射
     * Return columns map.
     */
    public Map<String, Object> getColumns() {
        if (null == columns) {
            columns = new HashMap<String, Object>();
        }
        return columns;
    }

    /**
     * 用 map 设置列值。
     * Set columns value with map.
     *
     * @param columns the columns map
     */
    private MyRecord setColumns(Map<String, Object> columns) {
        this.getColumns().putAll(columns);
        return this;
    }

    /**
     * 用 MyRecord 设置列的值。
     * Set columns value with MyRecord.
     *
     * @param record the MyRecord object
     */
    public MyRecord setColumns(MyRecord record) {
        this.getColumns().putAll(record.getColumns());
        return this;
    }

    /**
     * 用 JSONObject 设置列的值。
     * Set columns value with JSONObject.
     *
     * @param jsonObject the MyRecord object
     */
    public MyRecord setColumns(JSONObject jsonObject) {
        Map<String, Object> columns = this.columns;
        columns.putAll(jsonObject);
        return this;
    }

    /**
     * 用 Model 对象设置列的值。
     * Set columns value with Model object.
     *
     * @param t
     * @param <T>
     * @return
     */
    public <T> MyRecord setColumns(T t) {
        Map<String, Object> columns = this.getColumns();

        String[] fieldName = getFileName(t);

        for (int i = 0; i < fieldName.length; i++) {
            String name = fieldName[i];
            if (!StringUtils.isEmpty(name) && "serialVersionUID".equals(name)) {
                continue;
            }

            Object value = getFieldValueByName(name, t);
            if (null != value) {
                columns.put(name, value);
            }
        }
        return this;
    }

    /**
     * 删除这个myRecord的属性。
     * Remove attribute of this myRecord.
     *
     * @param column the column name of the myRecord
     */
    public MyRecord remove(String column) {
        getColumns().remove(column);
        return this;
    }

    /**
     * 删除这个myRecord的列。
     * Remove columns of this myRecord.
     *
     * @param columns the column names of the myRecord
     */
    public MyRecord remove(String... columns) {
        if (columns != null)
            for (String c : columns)
                this.getColumns().remove(c);
        return this;
    }

    /**
     * 如果为空，则删除列。
     * Remove columns if it is null.
     */
    public MyRecord removeNullValueColumns() {
        for (Iterator<Map.Entry<String, Object>> it = getColumns().entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, Object> e = it.next();
            if (e.getValue() == null) {
                it.remove();
            }
        }
        return this;
    }

    /**
     * 保留此记录的列，并删除其他列
     * Keep columns of this record and remove other columns.
     *
     * @param columns the column names of the record
     */
    public MyRecord keep(String... columns) {
        if (columns != null && columns.length > 0) {
            Map<String, Object> newColumns = new HashMap<>(columns.length);
            for (String c : columns)
                if (this.getColumns().containsKey(c))
                    newColumns.put(c, this.getColumns().get(c));

            this.getColumns().clear();
            this.getColumns().putAll(newColumns);
        } else
            this.getColumns().clear();

        return this;
    }

    /**
     * 保留该记录的一列，并删除其他列。
     * Keep column of this record and remove other columns.
     *
     * @param column the column names of the record
     */
    public MyRecord keep(String column) {
        if (getColumns().containsKey(column)) {
            Object keepIt = getColumns().get(column);
            getColumns().clear();
            getColumns().put(column, keepIt);
        } else
            getColumns().clear();
        return this;
    }

    /**
     * 删除此记录的所有列。
     * Remove all columns of this record.
     */
    public MyRecord clear() {
        getColumns().clear();
        return this;
    }

    /**
     * 设置列记录。
     * Set column to record.
     *
     * @param column the column name
     * @param value  the value of the column
     */
    public MyRecord set(String column, Object value) {
        getColumns().put(column, value);
        return this;
    }

    /**
     * 设置列记录。
     * Set column to record.
     *
     * @param column    the column name
     * @param valueList the value of the column
     */
    public MyRecord set(String column, List<MyRecord> valueList) {
        List<HashMap<String, Object>> value = new ArrayList<>();
        valueList.forEach(o -> {
            HashMap<String, Object> va = new HashMap<>(o.getColumns());
            value.add(va);
        });
        getColumns().put(column, value);
        return this;
    }

    /**
     * 获取任意 T 类型的列
     * Get column of any T type
     */
    public <T> T get(String column) {
        return (T) getColumns().get(column);
    }

    /**
     * 获取任意 T 类型的列。如果为空则返回 defaultValue。
     * Get column of any T type. Returns defaultValue if null.
     */
    public <T> T get(String column, Object defaultValue) {
        Object result = getColumns().get(column);
        return (T) (result != null ? result : defaultValue);
    }

    /**
     * 获取任意 Object 类型的列
     * Get column of any Object type
     */
    public Object getObject(String column) {
        return getColumns().get(column);
    }

    /**
     * 获取任意 Object 类型的列。如果为空则返回 defaultValue。
     * Get column of any Object type. Returns defaultValue if null.
     */
    public Object getObject(String column, Object defaultValue) {
        Object result = getColumns().get(column);
        return result != null ? result : defaultValue;
    }

    /**
     * 获取 mysql 类型的列: varchar, char, enum, set, text, tinytext, mediumtext, longtext
     * Get column of String type: varchar, char, enum, set, text, tinytext, mediumtext, longtext
     */
    public String getStr(String column) {
        Object result = getColumns().get(column);
        return result != null ? result.toString() : null;
    }

    /**
     * 获取 Integer  类型列: int, Integer, tinyint(n) n > 1, smallint, mediumint
     * Get column of Integer type: int, integer, tinyint(n) n > 1, smallint, mediumint
     */
    public Integer getInt(String column) {
        Number result = getNumber(column);
        return result != null ? result.intValue() : null;
    }

    /**
     * 获取 Long 类型的列: bigint
     * Get column of Long type: bigint
     */
    public Long getLong(String column) {
        Number result = getNumber(column);
        return result != null ? result.longValue() : null;
    }

    /**
     * 获取 BigInteger 类型的列: unsigned bigint
     * Get column of BigInteger type: unsigned bigint
     */
    public BigInteger getBigInteger(String column) {
        return (BigInteger) getColumns().get(column);
    }

    /**
     * 获取 Date 类型的列: date, year
     * Get column of Date type: date, year
     */
    public Date getDate(String column) {
        return (Date) getColumns().get(column);
    }

    /**
     * 获取 Time 类型的列: time
     * Get column of Time type: time
     */
    public Time getTime(String column) {
        return (Time) getColumns().get(column);
    }

    /**
     * 获取 Timestamp 类型的列: timestamp, datetime
     * Get column of Timestamp type: timestamp, datetime
     */
    public Timestamp getTimestamp(String column) {
        return (Timestamp) getColumns().get(column);
    }

    /**
     * 获取 Double 类型的列: real, double
     * Get column of Double type: real, double
     */
    public Double getDouble(String column) {
        Number n = getNumber(column);
        return n != null ? n.doubleValue() : null;
    }

    /**
     * 获取 Float 类型的列: float
     * Get column of Float type: float
     */
    public Float getFloat(String column) {
        Number n = getNumber(column);
        return n != null ? n.floatValue() : null;
    }

    /**
     * 获取 Short 类型的列: Short
     * Get column of Short type: Short
     */
    public Short getShort(String column) {
        Number n = getNumber(column);
        return n != null ? n.shortValue() : null;
    }

    /**
     * 获取 Byte 类型的列: Byte
     * Get column of Byte type: Byte
     */
    public Byte getByte(String column) {
        Number n = getNumber(column);
        return n != null ? n.byteValue() : null;
    }

    /**
     * 获取 Boolean 类型的列: bit, tinyint(1)
     * Get column of Boolean type: bit, tinyint(1)
     */
    public Boolean getBoolean(String column) {
        return (Boolean) getColumns().get(column);
    }

    /**
     * 获取 BigDecimal 类型的列: decimal, numeric
     * Get column of BigDecimal type: decimal, numeric
     */
    public BigDecimal getBigDecimal(String column) {
        Object n = getColumns().get(column);
        if (n instanceof BigDecimal) {
            return (BigDecimal) n;
        } else if (n != null) {
            return new BigDecimal(n.toString());
        } else {
            return null;
        }
    }

    /**
     * 获取byte[]类型的列:binary, varbinary, tinyblob, blob, mediumblob, longblob
     * Get column of byte[] type: binary, varbinary, tinyblob, blob, mediumblob, longblob
     * I have not finished the test.
     */
    public byte[] getBytes(String column) {
        return (byte[]) getColumns().get(column);
    }

    /**
     * 获取从 Number 扩展的任何类型的列
     * Get column of any type that extends from Number
     */
    public Number getNumber(String column) {
        if (getColumns().get(column) instanceof String) {
            try {
                return NumberFormat.getInstance().parse(getColumns().get(column).toString());
            } catch (ParseException e) {
                logger.error("类型转换错误e = {}", e.getMessage());
                e.printStackTrace();
            }
        }
        return (Number) getColumns().get(column);
    }

    /**
     * 返回该对象的字符串
     * Returns a string for the object
     */
    public String toString() {
        if (columns == null) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder();
        sb.append('{');
        boolean first = true;
        for (Map.Entry<String, Object> e : getColumns().entrySet()) {
            if (first) {
                first = false;
            } else {
                sb.append(", ");
            }
            Object value = e.getValue();
            if (value != null) {
                value = value.toString();
            }
            sb.append(e.getKey()).append(':').append(value);
        }
        sb.append('}');
        return sb.toString();
    }

    /**
     * 判断引用类型
     * Determine reference type
     *
     * @param o
     */
    public boolean equals(Object o) {
        if (!(o instanceof MyRecord))
            return false;
        if (o == this)
            return true;
        return getColumns().equals(((MyRecord) o).getColumns());
    }

    /**
     * 获取 hashCode 值
     * Gets the hashCode value
     */
    public int hashCode() {
        return getColumns().hashCode();
    }

    /**
     * 返回此记录的列名。
     * Return column names of this record.
     */
    public String[] getColumnNames() {
        Set<String> attrNameSet = getColumns().keySet();
        return attrNameSet.toArray(new String[0]);
    }

    /**
     * 返回此记录的列值。
     * Return column values of this record.
     */
    public Object[] getColumnValues() {
        java.util.Collection<Object> attrValueCollection = getColumns().values();
        return attrValueCollection.toArray(new Object[0]);
    }

    /**
     * 返回该记录的json字符串。
     * Return json string of this record.
     */
    public String toJson() {
        return JSON.toJSONString(getColumns());
    }
}
