package com.ncamc.common.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

/**
 * 商品品牌 VO 对象
 */
@Data
public class ShopBrandVo {

    /**
     * 品牌ID
     */
    @TableField(value = "brand_id")
    private Integer brandId;

    /**
     * 品牌名称
     */
    @TableField(value = "brand_wording")
    private String brandWording;

}
