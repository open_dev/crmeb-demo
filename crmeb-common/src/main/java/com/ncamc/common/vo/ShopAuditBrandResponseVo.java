package com.ncamc.common.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

/**
 * 店铺审核品牌响应
 */
@Data
public class ShopAuditBrandResponseVo extends BaseResultResponseVo {

    // 审核单id
    @TableField(value = "audit_id")
    private String auditId;
}
