package com.ncamc.common.vo;

import lombok.Data;

/**
 * 上传类目资质 itemData
 */
@Data
public class ShopAuditCategoryRequestItemDataVo {

    // 一级类目
    private Integer level1;

    // 二级类目
    private Integer level2;

    // 三级类目
    private Integer level3;

    // 资质材料
    private String certificate;
}
