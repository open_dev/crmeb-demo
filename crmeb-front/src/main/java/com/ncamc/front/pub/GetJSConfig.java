package com.ncamc.front.pub;

import com.ncamc.common.constants.Constants;
import com.ncamc.service.service.SystemConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Author : hugaoqiang 2023-11-10
 */
@Slf4j
@RestController
@RequestMapping("api/public/jsconfig")
@Api(tags = "公共JS配置")
public class GetJSConfig {

    @Resource
    private SystemConfigService systemConfigService;

    @PreAuthorize("hasAuthority('public:jsconfig:getcrmebchatconfig')")
    @ApiOperation(value = "CRMEB-chat客服统计")
    @RequestMapping(value = "/getcrmebchatconfig", method = RequestMethod.GET)
    public String set() {
        return systemConfigService.getValueByKey(Constants.JS_CONFIG_CRMEB_CHAT_TONGJI);
    }
}
