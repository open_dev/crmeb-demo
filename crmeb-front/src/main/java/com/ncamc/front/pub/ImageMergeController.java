package com.ncamc.front.pub;

import com.ncamc.common.response.CommonResult;
import com.ncamc.common.utils.ImageMergeUtil;
import com.ncamc.common.vo.ImageMergeUtilVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author : hugaoqiang 2023-11-10
 */
@Slf4j
@RestController
@RequestMapping("api/public/qrcode")
@Api(tags = "图片操作")
public class ImageMergeController {

    @ApiOperation(value = "合并图片返回文件")
    @RequestMapping(value = "/mergeList", method = RequestMethod.POST)
    public CommonResult<Map<String, String>> mergeList(@RequestBody @Validated List<ImageMergeUtilVo> list) {
        Map<String, String> map = new HashMap<>();
        // 需要云服务器域名，如果需要存入数据可参照上传图片服务
        map.put("base64Code", ImageMergeUtil.drawWordFile(list));
        return CommonResult.success(map);
    }

}
