package com.ncamc.front;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableAsync // 开启异步调用
@Configuration // 将想要的组件添加到容器中
@EnableTransactionManagement // 开启事物管理
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class) // 去掉数据库
@ComponentScan(basePackages = {"com.ncamc"}) //组件扫描
@MapperScan(basePackages = {"com.ncamc.**.dao"}) //配置包扫描
public class CrmebFrontApplication {
    public static void main(String[] args) {
        SpringApplication.run(CrmebFrontApplication.class);
    }
}
