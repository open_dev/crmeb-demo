package com.ncamc.front.filter;

import com.ncamc.common.constants.Constants;
import com.ncamc.common.utils.SpringUtil;
import com.ncamc.service.service.SystemAttachmentService;

/**
 * response路径处理
 *
 * @Author : hugaoqiang 2023-11-10
 */
public class ResponseRouter {

    public static String un() {
        return "";
    }

    public String filter(String data, String path) {
        boolean result = un().contains(path);
        if (result) {
            return data;
        }

        if (!path.contains("api/admin") && !path.contains("api/front")) {
            return data;
        }

        // 根据需要处理返回值
        if (data.contains(Constants.UPLOAD_TYPE_IMAGE + "/") && !data.contains("data:image/pnf;base64")) {
            data = SpringUtil.getBean(SystemAttachmentService.class).prefixImage(data);
        }

        if (data.contains("file/")) {
            data = SpringUtil.getBean(SystemAttachmentService.class).prefixFile(data);
        }

        return data;
    }

}
